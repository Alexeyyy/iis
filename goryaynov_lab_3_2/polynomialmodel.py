from regressionmodel import RegressionModel
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from matplotlib import pyplot as plt


class PolynomialModel(RegressionModel):

    def run(self, x_train, y_train, x_test, y_test):
        polynomial_regressor = PolynomialFeatures(degree=3)
        X_polynomial = polynomial_regressor.fit_transform(x_train)
        linear_regressor = LinearRegression()
        linear_regressor.fit(X_polynomial, y_train)
        y_pred = linear_regressor.predict(X_polynomial)
        plt.scatter(x_train, y_train, color='red')
        plt.plot(x_train, linear_regressor.predict(polynomial_regressor.fit_transform(x_train)), color='blue')
        plt.show()
        print('---Полиномиальная  регрессия---')
        print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_train, y_pred))
        print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_train, y_pred))

        