import pandas as pd

DATA = 'titanic.csv'
SEX_COLUMN = 'Sex'
PCLASS_COLUMN = 'Pclass'
SEX_VALUE = 'female'
PCLASS_VALUE = 2

data = pd.read_csv(DATA)


def get_females():
    # count = 0
    # for _, row in data.iterrows():
    # if (str(row[SEX_COLUMN]) == SEX_VALUE) & (str(row[PCLASS_COLUMN]) == PCLASS_VALUE):
    #    count += 1
    # return count
    return data.loc[(data[SEX_COLUMN] == SEX_VALUE) & (data[PCLASS_COLUMN] == PCLASS_VALUE)].shape[0]


print('Кол-во женщин во 2-ом классе: ' + str(get_females()))
