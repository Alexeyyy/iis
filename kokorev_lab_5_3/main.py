"""5. Кластеризация: реализация любого алгоритма кластеризации.
Сгенерировать случайный набор данных и реализовать на нем кластеризацию
любым понравившимся вам алгоритмом, кроме fcm. Ограничение – не менее трех алгоритмов на группу.
"""

from scipy.cluster.hierarchy import linkage, dendrogram
import matplotlib.pyplot as plt
import pandas as pd

seeds_df = pd.read_csv("data.csv")

# извлекаем измерения как массив NumPy
samples = seeds_df.values

# иерархическая кластеризация методо linkage
merge = linkage(samples, method='complete')

# строим дендрограмму, указав параметры удобные для отображения
dendrogram(merge, leaf_rotation=90, leaf_font_size=6)
plt.show()
