import pandas as p
from sklearn.tree import DecisionTreeClassifier

# Считываем файл
data = p.read_csv('titanic.csv')

# Убираем строки, где хотя бы в одном из
# столбцов есть пустое значение
def drop_values(data):
    return data[["Survived", "Parch", "Ticket", "Fare"]].dropna()

# В номере билета оставляем только цифры
def ticket_drop_let(str):
    result = ""
    for i in str:
        if i.isdigit():
            result = result + i
    return result

# Заменяем старый номер билета на новый,
# которыый состоит только из цифр
def ticket_number(data):
    for i in range(len(data["Ticket"])):
        data["Ticket"].update(data["Ticket"].replace(data["Ticket"][i], ticket_drop_let(data["Ticket"][i])))
    data["Ticket"] = p.to_numeric(data["Ticket"])
    return data


data = ticket_number(data)
data = drop_values(data)

# Выбираем данные
y = data["Survived"]
x = data[["Parch", "Ticket", "Fare"]]

clf = DecisionTreeClassifier()
# Обучение модели
clf.fit(x, y)

# Вывод названия признаков
print(clf.feature_names_in_)
# Вывод важности
print(clf.feature_importances_)