import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from iis.pikov_lab_6_16.Constants import DESCRIPTION_FEATURE_DELIMITER, LAST, SCORE, DEFAULT_RECOMMENDATIONS_COUNT, \
    INDEX_IN_DATASET, WITHOUT_FIRST


class Recommender:

    def __init__(self, filename, description_features):
        self._dataset = pd.read_csv(filename)
        self._description_features = description_features
        self._objects_descriptions = self.create_objects_descriptions()

    def create_objects_descriptions(self):
        return self._dataset[self._description_features]\
            .agg(DESCRIPTION_FEATURE_DELIMITER.join, axis=1)

    def fit(self, object_description, recommendations_count=DEFAULT_RECOMMENDATIONS_COUNT):
        similarity_scores = self.compute_similarity_scores_for_object_description(object_description)
        similarity_scores = self.get_top_n_recommendations(recommendations_count, similarity_scores)
        return self.find_objects_in_dataset_by_indices(similarity_scores)

    def compute_similarity_scores_for_object_description(self, object_description):
        similarity_matrix = self.create_similarity_matrix(object_description)
        return list(enumerate(similarity_matrix[LAST]))

    def create_similarity_matrix(self, object_description):
        object_description_as_pd_series = pd.Series(object_description)
        self._objects_descriptions = self._objects_descriptions.append(object_description_as_pd_series, ignore_index=True)
        vectorized_dataset = self.vectorize_and_remove_stop_words()
        similarity_matrix = cosine_similarity(vectorized_dataset)
        return similarity_matrix

    def vectorize_and_remove_stop_words(self):
        tfidf = TfidfVectorizer(stop_words="english")
        tfidf_matrix = tfidf.fit_transform(self._objects_descriptions)
        return tfidf_matrix

    def get_top_n_recommendations(self, recommendations_count, similarity_scores):
        similarity_scores = self.sort_similarity_scores_by_value_desc(similarity_scores)
        return similarity_scores[WITHOUT_FIRST:recommendations_count + 1]

    def sort_similarity_scores_by_value_desc(self, similarity_scores):
        return sorted(
            similarity_scores,
            key=lambda similarity: similarity[SCORE],
            reverse=True
        )

    def find_objects_in_dataset_by_indices(self, similarity_scores):
        only_matched_scores = filter(lambda similarity: similarity[SCORE] > 0, similarity_scores)
        indices_from_similarity_scores = [score[INDEX_IN_DATASET] for score in only_matched_scores]
        return self._dataset.iloc[indices_from_similarity_scores]
