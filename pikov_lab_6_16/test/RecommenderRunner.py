import unittest

from iis.pikov_lab_6_16.Constants import ARTIST_COLUMN_NAME, WRITERS_COLUMN_NAME, PRODUCER_COLUMN_NAME, RELEASE_DATE, \
    TITLE_COLUMN_NAME
from iis.pikov_lab_6_16.Helper import preety_print
from iis.pikov_lab_6_16.Recommender import Recommender


def create_recommender(features):
    dataset_filename = "../500_greatest_songs.csv"
    return Recommender(
        filename=dataset_filename,
        description_features=features
    )


class RecommenderRunner(unittest.TestCase):

    def test_recommender_by_group_name(self):
        recommender = create_recommender(
            features=[ARTIST_COLUMN_NAME, WRITERS_COLUMN_NAME, PRODUCER_COLUMN_NAME]
        )
        recommendations = recommender.fit("Beatles")
        print("\nRecommendations by group/person:")
        preety_print(recommendations)

    def test_recommender_by_year(self):
        recommender = create_recommender(
            features=[RELEASE_DATE]
        )
        recommendations = recommender.fit("1970")
        print("\nRecommendations by year:")
        preety_print(recommendations)

    def test_recommender_by_title(self):
        recommender = create_recommender(
            features=[TITLE_COLUMN_NAME]
        )
        recommendations = recommender.fit("Summer")
        print("\nRecommendations by title:")
        preety_print(recommendations)


if __name__ == '__main__':
    unittest.main()
