from tabulate import tabulate


def preety_print(recommendations):
    print(tabulate(recommendations, headers='keys', tablefmt='psql'))
