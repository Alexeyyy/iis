from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA


# Загружаем датасет
def load_dataset():
    dataset = load_iris()
    return dataset


# Определяем и обучаем модель
def dbscan(dataset):
    dbscan = DBSCAN()
    dbscan.fit(dataset.data)
    return dbscan


# Уменьшаем размерность при помощи метода главных компонент
def pca(dataset):
    pca = PCA(n_components=2).fit(dataset.data)
    pca_2d = pca.transform(dataset.data)
    return pca_2d


# Построение графика в соответствии с тремя классами
def show_result(pca_2d, dbscan):
    for i in range(0, pca_2d.shape[0]):
        if dbscan.labels_[i] == 0:
            first_cluster = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='red', marker=',')
        elif dbscan.labels_[i] == 1:
            second_cluster = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='blue', marker=',')
        elif dbscan.labels_[i] == -1:
            third_cluster = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='black', marker=',')
    plt.legend([first_cluster, second_cluster, third_cluster], ['Кластер 1', 'Кластер 2', 'Шум'])
    plt.title('Найдены 2 кластера и шум с помощью DBSCAN')
    plt.show()


data = load_dataset()
dbscan = dbscan(data)
pca = pca(data)
show_result(pca, dbscan)
