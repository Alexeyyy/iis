
# По данным о пассажирах Титаника решите задачу классификации (с помощью дерева решений),
# в которой по различным характеристикам пассажиров требуется найти у выживших пассажиров
# два наиболее важных признака из трех рассматриваемых.

# Признаки по варианту: Age, Fare, Embarked

import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot


def read_csv(path: str) -> pd.DataFrame:
    return pd.read_csv(path)


# Избавляемся от строк имеющих пустые значения хотябы в одном из столбцов (признаков) по варианту
def clear_noises(dataframe: pd.DataFrame):
    return dataframe[["Survived", "Age", "Fare", "Embarked"]].dropna()


dataframe_with_noises = read_csv('titanic.csv')
dataframe_without_noises = clear_noises(dataframe_with_noises)

classifier = DecisionTreeClassifier()

# Определяем зависимости
y = dataframe_without_noises["Survived"]
# Заменяем нечисловые значения признака Embarked, на числовые (значения выданы спонтанно)
x = dataframe_without_noises[["Age", "Fare"]].join(dataframe_without_noises["Embarked"].map({"S": 1, "C": 2, "Q": 3}))

classifier.fit(x, y)

# Выводим названия признаков и их важность
print(classifier.feature_names_in_)
print(classifier.feature_importances_)

# Визуализируем дерево решений
tree.plot_tree(classifier)
fig = pyplot.gcf()
fig.set_size_inches(9, 7)
pyplot.show()

