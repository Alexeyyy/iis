import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from typing import Final


GENRE: Final = 'genre'
NAME: Final = 'name'
NAME_DATASET: Final = 'anime.csv'
STOP_WORDS: Final = 'english'

data = pd.read_csv(NAME_DATASET, low_memory=False)
tf_idf = TfidfVectorizer(stop_words=STOP_WORDS)
data[GENRE] = data[GENRE].fillna('')
matrix = tf_idf.fit_transform(data[GENRE])
cosine = linear_kernel(matrix, matrix)
indices = pd.Series(data.index, index=data[NAME]).drop_duplicates()


def get_recommendation(title):
    x = indices[title]
    scores = list(enumerate(cosine[x]))
    scores = sorted(scores, key=lambda x: x[1], reverse=True)
    sim_scores = scores[1:11]
    movie_indices = [i[0] for i in sim_scores]
    return data[NAME].iloc[movie_indices]


print(get_recommendation('Cowboy Bebop'))
