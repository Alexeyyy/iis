import pandas as p
from sklearn.tree import DecisionTreeClassifier

dataTable = p.read_csv('titanic.csv')

SURVIVED = 'Survived'
TICKET = 'Ticket'
FARE = 'Fare'
CABIN = 'Cabin'

def drop_let(strDrop):
    result = ""

    for i in strDrop:
        if i.isdigit():
            result = result + i

    return result

def set_number(data, nameStr):
    for i in range(len(data[nameStr])):
        data[nameStr].update(data[nameStr].replace(data[nameStr][i], drop_let(str(data[nameStr][i]))))

    data[nameStr] = p.to_numeric(data[nameStr])

    return data

def drop_values(data):
    return data[[SURVIVED, TICKET, FARE, CABIN]].dropna()

dataTable = set_number(dataTable, TICKET)
dataTable = set_number(dataTable, CABIN)
dataTable = drop_values(dataTable)

y = dataTable[SURVIVED]
x = dataTable[[TICKET, FARE, CABIN]]

classifier = DecisionTreeClassifier()
classifier.fit(x, y)


print(classifier.feature_names_in_)
print(classifier.feature_importances_)
