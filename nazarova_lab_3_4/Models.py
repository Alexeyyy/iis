import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures


class Models:

    def linear_regression(self, x_learn, x_test, y_learn, y_test):
        lin_reg = LinearRegression().fit(x_learn, y_learn)
        y_pred = lin_reg.predict(x_test)
        print(lin_reg.score(x_learn, y_learn))

        plt.title("Линейная регрессия")
        plt.scatter(x_test, y_test, color='deeppink')
        plt.plot(x_test, y_pred, color='lawngreen', linewidth=2)
        plt.show()

    def polynomial_regression(self, x, y):
        poly_reg = PolynomialFeatures(degree=4)
        x_poly = poly_reg.fit_transform(x)
        pol_reg = LinearRegression()
        pol_reg.fit(x_poly, y)
        print(pol_reg.score(x_poly, y))

        plt.title("Полиномиальная регрессия")
        plt.scatter(x, y, color='b')
        plt.plot(x, pol_reg.predict(poly_reg.fit_transform(x)),
                 color='gold', linewidth=2)
        plt.show()

    def ridge_regression(self, x_ridge, x_test, y_ridge, y_test):
        clf = Ridge(alpha=1.0)
        clf.fit(x_ridge, y_ridge)
        y_pred = clf.predict(x_test)
        print(clf.score(x_ridge, y_ridge))

        plt.title("Гребневая полиномиальная регрессия")
        plt.scatter(x_test, y_test, color='m')
        plt.plot(x_test, y_pred, color='goldenrod', linewidth=2)
        plt.show()
