import numpy as np
from sklearn.datasets import make_moons
from Models import Models


models_manager = Models()


def get_dataset():
    return make_moons(noise=0.2, random_state=1)


def get_data(x, y):
    X1 = x[:75]
    X2 = x[75:]
    Y1 = y[:75]
    Y2 = y[75:]
    return X1, X2, Y1, Y2


def call_regression():
    models_manager.linear_regression(X1, X2, Y1, Y2)
    models_manager.polynomial_regression(X1, Y1)
    models_manager.ridge_regression(X1, X2, Y1, Y2)


dataset = get_dataset()
print(dataset)
x = np.array(dataset[0])
y = np.array(dataset[1])
x = np.delete(x, 0, 1)

X1, X2, Y1, Y2 = get_data(x, y)

call_regression()
