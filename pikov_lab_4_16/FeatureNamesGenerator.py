DEFAULT_FEATURE_NAME_PREFIX = 'x'
FEATURE_NUM_START_FROM = 1

def generate_feature_names(
        features_count,
        features_name_prefix=DEFAULT_FEATURE_NAME_PREFIX
):
    names = []
    for feature_num in range(FEATURE_NUM_START_FROM, features_count + FEATURE_NUM_START_FROM):
        feature_name = "%s%s" % (features_name_prefix, feature_num)
        names.append(feature_name)
    return names
