import numpy as np
from sklearn.preprocessing import MinMaxScaler

from iis.pikov_lab_4_16.Constants import X_DATA, Y_DATA
from iis.pikov_lab_4_16.FeatureNamesGenerator import generate_feature_names

RANK = 1


class ModelAnalyzer:

    def __init__(self, data):
        self._x = data[X_DATA]
        self._y = data[Y_DATA]

    def analyze(self, model):
        self.train_model(model)
        features_count = len(self._x)
        feature_ranks = self.rank_to_dict(model.coef_, generate_feature_names(features_count))
        self.sort_and_print_ranks(feature_ranks)

    def sort_and_print_ranks(self, feature_ranks):
        sorted_ranks = list(sorted(
            feature_ranks, key=lambda feature_rank: feature_rank[RANK], reverse=True
        ))
        print(sorted_ranks)

    def train_model(self, model):
        model.fit(self._x, self._y)

    def rank_to_dict(self, ranks, names):
        ranks = np.abs(ranks)
        minmax = MinMaxScaler()
        ranks = minmax.fit_transform(np.array(ranks).reshape(len(ranks), 1)).ravel()
        ranks = map(lambda x: round(x, 2), ranks)
        return zip(names, ranks)
