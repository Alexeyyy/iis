import unittest

from sklearn.linear_model import LinearRegression, Lasso

from iis.pikov_lab_4_16.FreedmanDatasetGenerator import generate_regression_freedman_problem_dataset
from iis.pikov_lab_4_16.ModelAnalyzer import ModelAnalyzer


class ModelAnalyzerRunner(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        data = generate_regression_freedman_problem_dataset()
        cls._model_analyzer = ModelAnalyzer(data)

    def test_linear_regression(self):
        print("\nМодель - Линейная регрессия:")
        model = LinearRegression()
        self._model_analyzer.analyze(model)

    def test_random_lasso(self):
        print("\nМодель - Слуйчайное Лассо:")
        model = Lasso(alpha=0.1, selection='random')
        self._model_analyzer.analyze(model)

    def test_f_regression(self):
        print("\nМодель - Линейная корреляция:")
        model = LinearRegression(fit_intercept=True)
        self._model_analyzer.analyze(model)


if __name__ == '__main__':
    unittest.main()
