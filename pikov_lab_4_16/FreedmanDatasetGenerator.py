import numpy as np

SEED = 1

DEFAULT_ROWS_COUNT = 750
DEFAULT_FEATURES_COUNT = 14

LOW_BOUNDARY_OF_GENERATED_VALUES = 0
HIGH_BOUNDARY_OF_GENERATED_VALUES = 1


def generate_regression_freedman_problem_dataset(
        rows_count=DEFAULT_ROWS_COUNT,
        features_count=DEFAULT_FEATURES_COUNT
):
    x = generate_x(features_count, rows_count)
    y = compute_y_by_freedman_regression_problem_formula(x)
    x = add_features_dependency(x, rows_count)
    return x, y


def generate_x(features_count, rows_count):
    np.random.seed(SEED)
    return np.random.uniform(
        LOW_BOUNDARY_OF_GENERATED_VALUES,
        HIGH_BOUNDARY_OF_GENERATED_VALUES,
        (rows_count, features_count)
    )


def compute_y_by_freedman_regression_problem_formula(x):
    return (10 * np.sin(np.pi * x[:, 0] * x[:, 1]) + 20 * (x[:, 2] - .5) ** 2 +
            10 * x[:, 3] + 5 * x[:, 4] ** 5 + np.random.normal(0, 1))


def add_features_dependency(x, rows_count):
    x[:, 10:] = x[:, :4] + np.random.normal(0, .025, (rows_count, 4))
    return x
