import pandas
data = pandas.read_csv("titanic.csv")

column_pclass = "Pclass"
column_sex = "Sex"

def get_number_men_in_first_class():
    count = 0
    for passenger in range(len(data[column_pclass])):
        if (data[column_pclass][passenger]) == 1 and (data[column_sex][passenger]) == "male":
            count += 1
    return count


print(get_number_men_in_first_class())
