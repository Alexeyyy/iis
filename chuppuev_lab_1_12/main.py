import pandas as pd

FILE_NAME = 'titanic.csv'
NUM_CLASS = 1
SEX_COL = 'Sex'
PCLASS_COL = 'Pclass'
SEX_MALE = 'male'


def load_file(file):
    return pd.read_csv(file)


data = load_file(FILE_NAME)


def get_males_count():
    return data[(data[SEX_COL] == SEX_MALE) & (data[PCLASS_COL] == NUM_CLASS)].shape[0]


print(str(get_males_count()) + ' ' + 'мужчин было в первом классе')
