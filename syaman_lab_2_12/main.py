import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot


def get_data(filename) -> DataFrame:
    return pd.read_csv(filename)


def print_results(classifier):
    print(classifier.feature_importances_)
    tree.plot_tree(classifier)
    pyplot.show()

def drop_let(str):
    result = ""
    for i in str:
        if i.isdigit():
            result = result + i
    return result

def set_number(data, var_name):
    for i in range(len(data[var_name])):
        data[var_name].update(
            data[var_name].replace(
                data[var_name][i],
                drop_let(str(data[var_name][i]))))
    data[var_name] = pd.to_numeric(data[var_name])
    return data


def get_dataframe():
    return pd.DataFrame(data=get_data('titanic.csv'), columns=['Age', 'Cabin', 'Embarked', 'Survived'])


def delete_none():
    return dataframe[['Age', 'Cabin', 'Embarked', 'Survived']].dropna()


dataframe = get_dataframe()
dataframe = set_number(dataframe, 'Cabin')
dataframe = delete_none()

classifier = DecisionTreeClassifier()

y = dataframe['Survived']
x = dataframe[['Age', 'Cabin']].join(dataframe['Embarked'].map({"S": 3, "C": 1, "Q": 2}))

classifier.fit(x, y)

print_results(classifier)
