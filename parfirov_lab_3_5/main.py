import numpy as np
from sklearn.datasets import make_circles
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures


def linear(x_train, x_test, y_train, y_test):
    model = LinearRegression().fit(x_train, y_train)
    y_pred = model.predict(x_test)

    print('Линейный коэффициент детерминации:', model.score(x_train, y_train))

    plt.title("Линейная регрессия")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_pred, color='red', linewidth=3)
    plt.show()


def polynomial(x, y):
    poly_reg = PolynomialFeatures(degree=4)
    x_poly = poly_reg.fit_transform(x)
    
    pol_reg = LinearRegression()
    pol_reg.fit(x_poly, y)
    
    print('Полиномиальный коэффициент детерминации:', pol_reg.score(x_poly, y))
    
    plt.title("Полиномиальная регрессия")
    plt.scatter(x, y, color='red')
    plt.plot(x, pol_reg.predict(poly_reg.fit_transform(x)), color='blue')
    plt.show()



def ridge(x_train, x_test, y_train, y_test):
    clf = Ridge(alpha=1.0)
    clf.fit(x_train, y_train)
    y_predict = clf.predict(x_test)

    print('Коэффициент определения гребня:', clf.score(x_train, y_train))

    plt.title("Гребневая полиномиальная регрессия")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_predict, color='red', linewidth=3)
    plt.show()


dataset = make_circles(noise=0.2, factor=0.5, random_state=1)

x = np.array(dataset[0])
y = np.array(dataset[1])

x = x[:, np.newaxis, 1]

x_train, x_test = x[:-50], x[-50:]
y_train, y_test = y[:-50], y[-50:]

linear(x_train, x_test, y_train, y_test)
polynomial(x_train, y_train)
ridge(x_train, x_test, y_train, y_test)
