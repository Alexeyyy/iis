import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

anime=pd.read_csv('anime.csv')
anime['genre'] = anime['genre'].fillna('')

Tfid=TfidfVectorizer(stop_words='english')
Tfid_matrix=Tfid.fit_transform(anime['genre'])
cosine_sim= linear_kernel(Tfid_matrix,Tfid_matrix)
indices=pd.Series(anime.index,index=anime['name']).drop_duplicates()

def getRecommendations(genre,cosine_sim=cosine_sim):
    idx=indices[genre]
    sim_scores = list(enumerate(cosine_sim[idx]))

    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

    sim_scores = sim_scores[1:11]

    anime_indeces = [i[0] for i in sim_scores]

    return anime['name'].iloc[anime_indeces]

print(getRecommendations('Noragami Aragoto'))