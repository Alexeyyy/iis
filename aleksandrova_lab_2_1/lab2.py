import pandas
from sklearn.tree import DecisionTreeClassifier
import numpy as np

PATH_TITANIC = 'titanic.csv'
INDEX_COL = 'PassengerId'
PCLASS_COL = 'Pclass'
NAME_COL = 'Name'
SEX_COL = 'Sex'
SURVIVED_COL = 'Survived'
MAX_LENGTH_STR = 255
COUNT_SYMBOLS_ALPHABET = 30
HASH_SPACE = MAX_LENGTH_STR * COUNT_SYMBOLS_ALPHABET


def load_data(path, index_col):
    return pandas.read_csv(path, index_col=index_col)


# функция для приведения пола к числу
def sex_to_bool(sex):
    if sex == "male":
        return 1
    elif sex == "female":
        return 0
    return np.NaN


# функция для приведения имени к числу
def name_to_number(name):
    return hash(name) % HASH_SPACE


data = load_data(PATH_TITANIC, INDEX_COL)
data[SEX_COL] = data[SEX_COL].apply(sex_to_bool)
data[NAME_COL] = data[NAME_COL].apply(name_to_number)
data = data.loc[(np.isnan(data[PCLASS_COL]) == False) & (np.isnan(data[NAME_COL]) == False)
                & (np.isnan(data[SEX_COL]) == False) & (np.isnan(data[SURVIVED_COL]) == False)]
x = data[[PCLASS_COL, NAME_COL, SEX_COL]]  # отбор столбцов
y = data[SURVIVED_COL]  # целевая переменная
model = DecisionTreeClassifier()
model.fit(x, y)
features = model.feature_names_in_
importances = model.feature_importances_
print('Признаки:')
print(features)
print('Важность признаков:')
print(importances)
