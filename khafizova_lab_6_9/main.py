import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

STYLE = 'Style'
VARIETY = 'Variety'
STOP_WORDS = 'english'

# Подготовка данных
data = pd.read_csv('ramen-ratings.csv', low_memory=False, )
tf_idf = TfidfVectorizer(stop_words=STOP_WORDS)
data[STYLE] = data[STYLE].fillna('')
matrix = tf_idf.fit_transform(data[STYLE])
cosine = linear_kernel(matrix, matrix)
indices = pd.Series(data.index, index=data[VARIETY]).drop_duplicates()

# Релизация алгоритма рекомендаций
x = indices['Paegaejang Ramen']
scores = list(enumerate(cosine[x]))
scores = sorted(scores, key=lambda x: x[1], reverse=True)
sim_scores = scores[1:11]
ramen_index = [i[0] for i in sim_scores]

print(data[VARIETY].iloc[ramen_index])
