import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot

file = 'titanic.csv'
pclass = 'Pclass'
age = 'Age'
ticket = 'Ticket'
survived = 'Survived'

def load_file(file) -> DataFrame:
     return pd.read_csv(file)

def get_dataframe():
     return pd.DataFrame(data=load_file(file), columns=[pclass, age, ticket, survived])

def delete_none():
     return dataframe[[pclass, age, ticket, survived]].dropna()

def delete_let():
    dataframe[ticket].update(dataframe[ticket].str.replace(r'\D', '', regex=True))
    dataframe[ticket] = pd.to_numeric(dataframe[ticket])
    return dataframe

def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)

    tree.plot_tree(classifier)
    fig = pyplot.gcf()
    fig.set_size_inches(9, 6)
    pyplot.show()


dataframe = get_dataframe()
dataframe = delete_let()
dataframe = delete_none()

y = dataframe[survived]
x = dataframe[[pclass, age, ticket]]

classifier = DecisionTreeClassifier()

classifier.fit(x, y)

print_results(classifier)
