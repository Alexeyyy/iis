import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics
from sklearn.linear_model import LinearRegression, Perceptron
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import PolynomialFeatures

class Models:

    def linear(self, X_TRAIN, X_TEST, Y_TRAIN, Y_TEST):
        model = LinearRegression().fit(X_TRAIN, Y_TRAIN)
        y_pred = model.predict(X_TEST)

        print(model.score(X_TRAIN, Y_TRAIN))
        print('')
        plt.scatter(X_TEST, Y_TEST, color='black')
        plt.plot(X_TEST, y_pred, color='blue', linewidth=3)
        plt.show()

    def polynomial(self, X, Y):
        poly_reg = PolynomialFeatures(degree=3)
        X_poly = poly_reg.fit_transform(X)
        pol_reg = LinearRegression()
        pol_reg.fit(X_poly, Y)

        print(pol_reg.score(X_poly, Y))
        print(" ")
        plt.scatter(X, Y, color='red')
        plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
        plt.show()

    def multi_layer_perceptron(self,X_TRAIN, Y_TRAIN, X_TEST):
        model = MLPClassifier(hidden_layer_sizes=1000, alpha=0.01)
        model.fit(X_TRAIN, Y_TRAIN)

        print(model.score(X_TRAIN, Y_TRAIN))
        plt.scatter(X_TRAIN, Y_TRAIN, color='blue')
        plt.scatter(X_TRAIN, model.predict(X_TEST), color='red')
        plt.show()
