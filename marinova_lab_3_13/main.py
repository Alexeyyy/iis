import numpy as np
from sklearn.datasets import make_moons
from Models import Models

models_manager = Models()

data = make_moons(noise=0.3,random_state=1)

X = np.array(data[0])
Y = np.array(data[1])

X = X[:, np.newaxis, 1]

X_TRAIN = X[:65]
X_TEST = X[-65:]
Y_TRAIN =Y[:65]
Y_TEST = Y[-65:]

models_manager.linear(X_TRAIN, X_TEST, Y_TRAIN, Y_TEST)

models_manager.polynomial(X_TRAIN, Y_TRAIN)

models_manager.multi_layer_perceptron(X_TRAIN, Y_TRAIN, X_TEST)
