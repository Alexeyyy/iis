# Использованные методы ранжирования:
# Лассо (Lasso)
# Сокращение признаков Случайными деревьями (Random Forest Regressor)
# Линейная корреляция (f_regression)


from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Lasso
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_selection import f_regression
import numpy as np


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def sort_rank(rank):
    return {k: v for k, v in sorted(rank.items(), key=lambda item: item[1])}


def print_ranks(ranks):
    for i in range(len(ranks)):
        print(sort_rank(ranks[i]))


np.random.seed(0)

size = 750
X = np.random.uniform(0, 1, (size, 14))
Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 + 10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))

rand_forest_reg = RandomForestRegressor(random_state=0)
rand_forest_reg.fit(X, Y)

lasso = Lasso(alpha=.001)
lasso.fit(X,  Y)

f_reg = f_regression(X, Y)
f_reg = f_reg[0]

names = ["x%s" % i for i in range(1, 15)]
ranks = [
    rank_to_dict(lasso.coef_, names),
    rank_to_dict(rand_forest_reg.feature_importances_, names),
    rank_to_dict(f_reg, names)
]

print_ranks(ranks)
