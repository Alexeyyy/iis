import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot


def get_data(filename) -> DataFrame:
    return pd.read_csv(filename)


def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)
    tree.plot_tree(classifier)
    pyplot.show()


def get_dataframe():
    return pd.DataFrame(data=get_data('titanic.csv'), columns=['Fare', 'Parch', 'Pclass', 'Survived'])


def delete_none():
    return dataframe[['Fare', 'Parch', 'Pclass', 'Survived']].dropna()


dataframe = get_dataframe()
dataframe = delete_none()
classifier = DecisionTreeClassifier()

y = dataframe['Survived']
x = dataframe[['Fare', 'Parch', 'Pclass']]

classifier.fit(x, y)

print_results(classifier)
