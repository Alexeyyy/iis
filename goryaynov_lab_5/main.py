import matplotlib.pyplot as plt
from sklearn.datasets import make_circles
from sklearn.cluster import KMeans

def show_scalar_plot(X, label: str, colors):
    plt.scatter(X[:, 0], X[:, 1], c=colors, marker='*', label=label)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(loc=0)
    plt.show()

X, _ = make_circles(n_samples=1000, noise=0.1)
show_scalar_plot(X, 'Датасет', 'red')
k_means_estimator = KMeans(n_clusters=2)
k_means_estimator.fit(X)
show_scalar_plot(X, 'Результат определения 2-х кластеров', k_means_estimator.labels_)
