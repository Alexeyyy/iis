from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures


class Models:

    def linear_regression(self, x_learn, x_test, y_learn, y_test):
        lin_reg = LinearRegression().fit(x_learn, y_learn)
        y_pred = lin_reg.predict(x_test)
        print(lin_reg.score(x_learn, y_learn))

        plt.title("linear regression")
        plt.scatter(x_test, y_test, color='black')
        plt.plot(x_test, y_pred, color='yellow', linewidth=2)
        plt.show()

    def polynomial_regression(self, x, y):
        poly_reg = PolynomialFeatures(degree=4)
        x_poly = poly_reg.fit_transform(x)
        pol_reg = LinearRegression()
        pol_reg.fit(x_poly, y)
        print(pol_reg.score(x_poly, y))

        plt.title("polynomial regression")
        plt.scatter(x, y, color='violet')
        plt.plot(x, pol_reg.predict(poly_reg.fit_transform(x)),
                 color='turquoise', linewidth=2)
        plt.show()

    def ridge_regression(self, x_ridge, x_test, y_ridge, y_test):
        clf = Ridge(alpha=1.0)
        clf.fit(x_ridge, y_ridge)
        y_pred = clf.predict(x_test)
        print(clf.score(x_ridge, y_ridge))

        plt.title("ridge polynomial regression")
        plt.scatter(x_test, y_test, color='forestgreen')
        plt.plot(x_test, y_pred, color='deepskyblue', linewidth=2)
        plt.show()