"""Влияние регуляризации на многослойную сеть прямого распространения

Используя код из пункта «Регуляризация и сеть прямого распространения»,
сгенерируйте определенный тип данных и сравните на нем 3 модели (по варианту).
Постройте графики, отобразите качество моделей, объясните полученные результаты.

Данные: make_classification (n_samples=500, n_features=2, n_redundant=0, n_informative=2, random_state=rs, n_clusters_per_class=1)
Модели:

- Линейную регрессию
- Полиномиальную регрессию (со степенью 3)
- Гребневую полиномиальную регрессию (со степенью 3, alpha = 1.0)

"""


import numpy as np
from sklearn.datasets import make_classification
from models import Models

models_manager = Models()


def get_dataset():
    return make_classification(n_samples=500,
                               n_features=2,
                               n_redundant=0,
                               n_informative=2,
                               random_state=1,
                               n_clusters_per_class=1)


def get_data(x, y):
    x_train = x[:30]
    x_test = x[30:]
    y_train = y[:30]
    y_test = y[30:]
    return x_train, x_test, y_train, y_test


def call_regression():
    models_manager.linear_regression(x_train, x_test, y_train, y_test)
    models_manager.polynomial_regression(x_train, y_train)
    models_manager.ridge_regression(x_train, x_test, y_train, y_test)


dataset = get_dataset()
print(dataset)
x = np.array(dataset[0])
y = np.array(dataset[1])
x = x[:, np.newaxis, 1]

x_train, x_test, y_train, y_test = get_data(x, y)

call_regression()
