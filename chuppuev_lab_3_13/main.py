import numpy as np
from sklearn.datasets import make_moons
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import PolynomialFeatures


data = make_moons(noise=0.3, random_state=1)

X = np.array(data[0])
Y = np.array(data[1])

X = X[:, np.newaxis, 1]

X_train = X[:55]
X_test = X[-55:]
Y_train = Y[:55]
Y_test = Y[-55:]


# x1-X_train, x2-X_test, y1-Y_train, y2-Y_test
def func_linear(x1, x2, y1, y2):
    model = LinearRegression().fit(x1, y1)
    y_pred = model.predict(x2)
    print('Линейная регрессия')
    print(model.score(x1, y1))
    print(" ")
    plt.scatter(x2, y2, color='black')
    plt.plot(x2, y_pred, color='green')
    plt.show()


def func_polynomial(x1, y1):
    poly_reg = PolynomialFeatures(degree=3)
    x_pol = poly_reg.fit_transform(x1)
    pol_reg = LinearRegression()
    pol_reg.fit(x_pol, y1)
    print('Полиноминальная регрессия')
    print(pol_reg.score(x_pol, y1))
    print(" ")
    plt.scatter(x1, y1, color='black')
    plt.plot(x1, pol_reg.predict(poly_reg.fit_transform(x1)), color='green')
    plt.show()


def func_mn_perceptron(x1, y1, x2):
    model = MLPClassifier(hidden_layer_sizes=1000, alpha=0.01)
    model.fit(x1, y1)
    print('Многослойный персептрон')
    print(model.score(x1, y1))
    print(" ")
    plt.scatter(x1, y1, color='black')
    plt.scatter(x1, model.predict(x2), color='green')
    plt.show()


func_linear(X_train, X_test, Y_train, Y_test)
func_polynomial(X_train, Y_train)
func_mn_perceptron(X_train, Y_train, X_test)
