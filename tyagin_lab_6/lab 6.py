import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel


TYPE = 'Type 2'
NAME = 'Name'
DATASET = 'Pokemon.csv'
STOP_WORDS = 'english'
MY_POKEMON = 'Weedle'

data = pd.read_csv(DATASET, low_memory=False)
data[TYPE] = data[TYPE].fillna('')

tf_idf = TfidfVectorizer(stop_words=STOP_WORDS)
matrix = tf_idf.fit_transform(data[TYPE])
cosine = linear_kernel(matrix, matrix)
indices = pd.Series(data.index, index=data[NAME]).drop_duplicates()

x = indices[MY_POKEMON]
scores = list(enumerate(cosine[x]))
scores = sorted(scores, key=lambda x: x[1], reverse=True)
sim_scores = scores[1:11]
poke_index = [i[0] for i in sim_scores]


print(data[NAME].iloc[poke_index])
