from sklearn.linear_model import Ridge
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import f_regression


def sort(data):
    sorted_values = sorted(data.values())
    sorted_data = {}

    for i in sorted_values:
        for j in data.keys():
            if data[j] == i:
                sorted_data[j] = data[j]
                break
    return sorted_data


# на вход получаем сформированный список и список оценок по признакам
# на выход словарь, составленный из попарно соотнесенных элементов списков
def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


# генерируем исходные данные: 750 строк-наблюдений и 14 столбцов-признаков
np.random.seed(0)
size = 750
X = np.random.uniform(0, 1, (size, 14))
# задаем функцию-выход: регрессионную проблему Фридмана
Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 + 10 * X[:, 3] + 5 * X[:, 4] ** 5
     + np.random.normal(0, 1))
# добавляем зависимость признаков
X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))

# гребневая регрессия
ridge = Ridge(alpha=7)
ridge.fit(X, Y)

# линейная корреляция
fr = f_regression(X, Y)
fr = fr[0]

# сокращение признаков Случайными деревьями
rfr = RandomForestRegressor(random_state=0)
rfr.fit(X, Y)
# список вида ['x1', 'x2', 'x3',..., 'x14'], содержащий имена признаков
names = ["x%s" % i for i in range(1, 15)]

rank = [rank_to_dict(ridge.coef_, names),
        rank_to_dict(fr, names),
        rank_to_dict(rfr.feature_importances_, names), ]

for i in range(len(rank)):
    print(sort(rank[i]))
