import pandas as pd
from IPython.display import display
import warnings
warnings.filterwarnings("ignore")

COLUMN_MOVIE_ID = 'movieId'
COLUMN_TITLE = 'title'
COLUMN_RATING = 'rating'
COLUMN_RATING_COUNTS = 'rating_counts'
COLUMN_USER_ID = 'userId'
COLUMN_CORRELATION = 'Correlation'

movie = 'movies.csv'
ratings = 'ratings.csv'


# импорт датасетов
def read_csv(filename):
    return pd.read_csv(filename)


# объединение двух датафреймов
def merge_frames(ratings_data, movie_names):
    movie_data = pd.merge(ratings_data, movie_names, on=COLUMN_MOVIE_ID)
    movie_data.groupby(COLUMN_TITLE)[COLUMN_RATING].count().sort_values(ascending=False)
    return movie_data


# создание датафрейма для объединения атрибутов: средний рейтинг и количество рейтингов
def rating_count(movie_data):
    ratings_mean_count = pd.DataFrame(movie_data.groupby(COLUMN_TITLE)[COLUMN_RATING].mean())
    ratings_mean_count[COLUMN_RATING_COUNTS] = pd.DataFrame(movie_data.groupby(COLUMN_TITLE)[COLUMN_RATING].count())
    return ratings_mean_count


# создание матрицы для корреляции между рейтингами фильмов
def generate_matrix(movie_data, movie_name):
    user_movie_rating = movie_data.pivot_table(index=COLUMN_USER_ID, columns=COLUMN_TITLE, values=COLUMN_RATING)
    forrest_gump_ratings = user_movie_rating[movie_name]
    movies_like_forest_gump = user_movie_rating.corrwith(forrest_gump_ratings)
    return movies_like_forest_gump


# вывод фильмов, которые сильно коррелируют с передаваемым фильмом
def correlation_user_ratings(matrix, ratings_mean_count):
    corr_forrest_gump = pd.DataFrame(matrix, columns=[COLUMN_CORRELATION])
    corr_forrest_gump = corr_forrest_gump.join(ratings_mean_count[COLUMN_RATING_COUNTS])
    display(corr_forrest_gump[corr_forrest_gump[COLUMN_RATING_COUNTS] > 30].sort_values
            (COLUMN_CORRELATION, ascending=False).head())


def main():
    movie_name = 'Titanic (1997)'
    rating_data = read_csv(ratings)
    movie_names = read_csv(movie)
    movie_data = merge_frames(rating_data, movie_names)
    ratings_mean_count = rating_count(movie_data)
    matrix = generate_matrix(movie_data, movie_name)
    correlation_user_ratings(matrix, ratings_mean_count)


if __name__ == '__main__':
    main()


