import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

class Regressions:

    def linear(self, X_train, X_test, Y_train, Y_test):
        model = LinearRegression().fit(X_train, Y_train)
        Y_pred = model.predict(X_test)
        print('___Линейная регрессия___')
        print('Средняя абсолютная ошибка:', round(metrics.mean_absolute_error(Y_test, Y_pred), 2))
        print('Корень из среднеквадратичной ошибки:', round(np.sqrt(metrics.mean_squared_error(Y_test, Y_pred)), 2))
        print('')

        self.show_plot(X_test, Y_test, Y_pred)

    def polynomial(self, X, Y):
        # степень = 4
        poly_reg = PolynomialFeatures(degree=4)
        X_poly = poly_reg.fit_transform(X)
        pol_reg = LinearRegression()
        pol_reg.fit(X_poly, Y)
        Y_pred = pol_reg.predict(X_poly)
        print('___Полиномиальная  регрессия степени 4___')
        print('Средняя абсолютная ошибка:', round(metrics.mean_absolute_error(Y, Y_pred), 2))
        print('Корень из среднеквадратичной ошибки:', round(np.sqrt(metrics.mean_squared_error(Y, Y_pred)), 2))
        print('')

        self.show_plot(X, Y, Y_pred)

    def ridge(self, X_train, X_test, Y_train, Y_test):
        # степень 4, = alpha = 1.0
        model = Pipeline([('poly', PolynomialFeatures(degree=4)), ('ridge', Ridge(alpha=1.0))])
        #clf = Ridge(alpha=1.0)
        model.fit(X_train, Y_train)
        Y_pred = model.predict(X_test)

        print('___Гребневая полиномиальная регрессия___')
        print('Средняя абсолютная ошибка:', round(metrics.mean_absolute_error(Y_test, Y_pred), 2))
        print('Корень из среднеквадратичной ошибки:', round(np.sqrt(metrics.mean_squared_error(Y_test, Y_pred)), 2))
        self.show_plot(X_test, Y_test, Y_pred)

    def show_plot(self, X_test, Y_test, Y_pred):
        plt.scatter(X_test, Y_test)
        plt.plot(X_test, Y_pred)
        plt.show()
