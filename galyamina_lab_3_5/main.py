import numpy as np
from sklearn.datasets import make_circles
from Regressions import Regressions

data = make_circles(noise=0.2, factor=0.5, random_state=1)
print(data)

X = np.array(data[0])
y = np.array(data[1])
X = X[:, np.newaxis, 1]

# делим набор данных на тренировочный и тестовый
X_train = X[:-10]
X_test = X[-10:]
y_train = y[:-10]
y_test = y[-10:]

regs = Regressions()
#Линейная регрессия
regs.linear(X_train, X_test, y_train, y_test)
#Полиномиальная регрессия
regs.polynomial(X_train, y_train)
#Гребневая полиномиальная регрессия
regs.ridge(X_train, X_test, y_train, y_test)
