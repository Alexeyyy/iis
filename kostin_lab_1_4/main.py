import pandas


def read_file(file):
    return pandas.read_csv(file)


def get_mean(df: pandas.DataFrame, column_name):
    return df[column_name].mean()


def get_median(df: pandas.DataFrame, column_name):
    return df[column_name].median()


file = read_file("titanic.csv")

print(f"Mean: {get_mean(file, 'Age')} \nMedian: {get_median(file, 'Age')}")