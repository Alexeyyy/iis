from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from sklearn.feature_selection import f_regression

np.random.seed(0)
size = 750
X = np.random.uniform(0, 1, (size, 14))
Y = (10 * np.sin(np.pi*X[:,0]*X[:,1]) + 20*(X[:,2] - .5)**2 + 10*X[:,3] + 5*X[:,4]**5 + np.random.normal(0,1))
X[:,10:] = X[:,:4] + np.random.normal(0, .025, (size,4))


lr = LinearRegression()
lr.fit(X, Y)

ridge = Ridge(alpha=5)
ridge.fit(X, Y)

lasso = Lasso(alpha=.05)
lasso.fit(X, Y)
fr = f_regression(X, Y)
fr = fr[0]

names = ["x%s" % i for i in range(1,15)]

def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14,1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))

ranks={}
ranks["Линейная регрессия"] = rank_to_dict(lr.coef_, names)
ranks["Гребневая регрессия"] = rank_to_dict(ridge.coef_, names)
ranks["Лассо"] = rank_to_dict(lasso.coef_, names)
ranks["Линейная корреляция"] = rank_to_dict(fr, names)
mean = {}
for key, value in ranks.items():
    for item in value:
        if(item not in mean):
            mean[item] = 0
        mean[item] += value[item]
for value in mean:
    res=mean[value]/len(ranks)
    mean[value] = round(res, 2)
mean = {k: v for k, v in sorted(mean.items(), key=lambda item: item[1])}

print (mean)

for key in ranks:
    ranks[key] = {k: v for k, v in sorted(ranks[key].items(), key=lambda item: item[1])}
for key in ranks:
    print (key)
    print (ranks[key])