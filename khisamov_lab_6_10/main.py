import pandas as pd
from pandas import DataFrame

number_similar = 3
CAR_NAME = 0
SIMILAR_NAME = 1
PERCENT = 2


def similarity_data(df):
    rec = DataFrame()
    rec_list = list()
    for i in df:
        k = 0
        corr_matrix = df.corrwith(df[i])
        corr_matrix = DataFrame(corr_matrix)
        temp_matrix = corr_matrix.drop([i], axis=0)
        rec_list.extend(rec_with_k(k, i, temp_matrix, number_similar))
    rec = rec.append(rec_list, ignore_index=True)
    return rec


def rec_with_k(k, i, temp_matrix, number_similar):
    sub_list = []
    while k != number_similar:
        sub_list.append(
            [i, temp_matrix.idxmax().item(), round(temp_matrix[0][temp_matrix.idxmax().item()], 2) * 100])
        temp_matrix = temp_matrix.drop([temp_matrix.idxmax().item()], axis=0)
        k += 1
    return sub_list


def search(car_name, rec):
    print("По оценкам водителей машины " + str(car_name) + " схожи с: ")
    for i in range(len(rec)):
        if rec[CAR_NAME][i] == car_name:
            print(str(rec[SIMILAR_NAME][i]) + "(на " + str(rec[PERCENT][i]) + "%)")


data = pd.read_excel('CarsData.xlsx', index_col=0)
similar_cars = similarity_data(data)

search('Toyota', similar_cars)
