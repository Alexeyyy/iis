
#Признаки: Pclass, Age, Ticket

import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot

# Читаем файл
def load_file(file) -> DataFrame:
     return pd.read_csv(file)

# Загружаем файл, выбираем столбцы по необходимым признакам 
def get_dataframe():
     return pd.DataFrame(data=load_file('titanic.csv'), columns=['Pclass', 'Age', 'Ticket', 'Survived'])

# Убираем пустые строки, если содержатся пропуски по любому из признаков
def delete_none():
     return dataframe[['Pclass', 'Age', 'Ticket', 'Survived']].dropna()

# Убираем буквы и оставляем только числа в номере билета
def delete_let():
    dataframe['Ticket'].update(dataframe['Ticket'].str.replace(r'\D', '', regex=True))
    dataframe["Ticket"] = pd.to_numeric(dataframe['Ticket'])
    return dataframe

# Важность, названия признаков. Визуализация дерева.
def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)

    tree.plot_tree(classifier)
    fig = pyplot.gcf()
    fig.set_size_inches(9, 6)
    pyplot.show()


dataframe = get_dataframe()
dataframe = delete_let()
dataframe = delete_none()

# Выбираем данные
y = dataframe['Survived']
x = dataframe[['Pclass', 'Age', 'Ticket']]

classifier = DecisionTreeClassifier()

# Обучение модели
classifier.fit(x, y)

print_results(classifier)
