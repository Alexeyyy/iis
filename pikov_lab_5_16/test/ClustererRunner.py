import unittest

from sklearn.datasets import load_iris

from iis.pikov_lab_5_16.Clusterer import Clusterer
from iis.pikov_lab_5_16.Helper import take_first_two_features, show_2D_scatter_highlight_clusters


class ClustererRunner(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._clusterer = Clusterer()

    def test_clusterer_on_iris_dataset(self):
        clusters_count = 7
        dataset = load_iris()
        X = take_first_two_features(dataset.data)

        clustering_result = self._clusterer.fit(
            data=X,
            clusters_count=clusters_count
        )

        show_2D_scatter_highlight_clusters(X, clustering_result)


if __name__ == '__main__':
    unittest.main()
