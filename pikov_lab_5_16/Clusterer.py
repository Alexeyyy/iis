from sklearn.cluster import SpectralClustering


class Clusterer:

    def fit(self, data, clusters_count):
        clusterer = SpectralClustering(
            n_clusters=clusters_count,
            assign_labels='discretize',
            random_state=0
        )
        clustering_result = clusterer.fit(data).labels_
        return clustering_result
