from matplotlib import pyplot as plt

from iis.pikov_lab_5_16.Constants import X_DATA, Y_DATA


def take_first_two_features(data):
    return data[:, :2]


def show_2D_scatter_highlight_clusters(X, clustering_result):
    plt.scatter(X[:, X_DATA], X[:, Y_DATA], c=clustering_result)
    plt.show()