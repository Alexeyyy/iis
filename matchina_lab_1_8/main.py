import pandas as p

data = p.read_csv('titanic.csv')


def count_not_survived():
    not_survived = 0
    count = 0
    for _, row in data.iterrows():
        count += 1
        if str(row['Survived']) == '0':
            not_survived += 1

    res = round((not_survived / count) * 100, 2)
    return res


print('Не удалось выжить ' + str(count_not_survived()) + ' %')
