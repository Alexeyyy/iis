"""По данным о пассажирах Титаника решите задачу классификации (с помощью дерева решений),
 в которой по различным характеристикам пассажиров требуется найти у выживших пассажиров 
 два наиболее важных признака из трех рассматриваемых: Ticket, Fare, Cabin"""

import pandas as p
from sklearn.tree import DecisionTreeClassifier

data = p.read_csv('titanic.csv')

SURVIVED = 'Survived'
TICKET = 'Ticket'
FARE = 'Fare'
CABIN = 'Cabin'

def drop_values(data):
    """убираем строки с пустыми значениями"""
    return data[[SURVIVED, TICKET, FARE, CABIN]].dropna()

def drop_let(str):
    """оставляем только цифры"""
    result = ""
    for i in str:
        if i.isdigit():
            result = result + i
    return result

def set_number(data, var_name):
    """заменяем номер"""
    for i in range(len(data[var_name])):
        data[var_name].update(
            data[var_name].replace(
                data[var_name][i], 
                drop_let(str(data[var_name][i]))))
    data[var_name] = p.to_numeric(data[var_name])
    return data


data = set_number(data, TICKET)
data = set_number(data, CABIN)
data = drop_values(data)

y = data[SURVIVED] # выбираем данные
x = data[[TICKET, FARE, CABIN]] # выбираем данные

clf = DecisionTreeClassifier()
clf.fit(x, y)  # обучение модели 


print(clf.feature_names_in_)  # выводим названия признаков
print(clf.feature_importances_)  # выводим важности
