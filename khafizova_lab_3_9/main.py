import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import Perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.datasets import make_classification


def do_perceptron(x_train, y_train, x_test):
    perceptron = Perceptron()
    perceptron.fit(x_train, y_train.ravel())

    print('Средняя точность:', perceptron.score(x_train, y_train))
    plt.title("График персептрона")
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, perceptron.predict(x_test), color='red', linewidth=3)
    plt.show()


def do_perceptron_multilayer_10(x_train, y_train, x_test):
    perceptron = MLPClassifier(hidden_layer_sizes=10, alpha=0.01)
    perceptron.fit(x_train, y_train.ravel())

    print('Средняя точность:', perceptron.score(x_train, y_train))
    plt.title("График многослойного персептрона с 10-ю нейронами")
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, perceptron.predict(x_test), color='red', linewidth=3)
    plt.show()


def do_perceptron_multilayer_100(x_train, y_train, x_test):
    perceptron = MLPClassifier(hidden_layer_sizes=100, alpha=0.01)
    perceptron.fit(x_train, y_train.ravel())

    print('Средняя точность:', perceptron.score(x_train, y_train))
    plt.title("График многослойного персептрона со 100-а нейронами")
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, perceptron.predict(x_test), color='red', linewidth=3)
    plt.show()


# Подгружаем датасет и распределяем данные на X, Y
dataset = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, random_state=1,
                              n_clusters_per_class=1)
print(dataset)

x = np.array(dataset[0])
y = np.array(dataset[1])
# Преобразуем двумерный массив в одномерный
x = x.reshape(-1, 1)
y = y.reshape(-1, 1)
# 100 значений для тренировки, 100 для теста
x_train, x_test = x[:100], x[-100:]
y_train, y_test = y[:100], y[-100:]
do_perceptron(x_train, y_train, x_test)
do_perceptron_multilayer_10(x_train, y_train, x_test)
do_perceptron_multilayer_100(x_train, y_train, x_test)

