import pandas as pd

sex = 'Sex'
female = 'female'
pclass ='Pclass'
file = 'titanic.csv'

one_class = 1

data = pd.read_csv(file);

def count_females():
    count = data[(data[sex] == female) & (data[pclass] == one_class)].shape[0]
    return count

print('Количество женщин в первом классе' + ' ' + str(count_females()))
