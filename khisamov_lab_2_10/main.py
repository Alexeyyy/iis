import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from pandas.core.frame import DataFrame

SURVIVED = 'Survived'
EMBARKED = 'Embarked'
NAME = 'Name'
CABIN = 'Cabin'


# чтение таблицы
def get_data(filename) -> DataFrame:
    return pd.read_csv(filename)


# получение DataFrame
def get_dataframe():
    return pd.DataFrame(data=get_data('titanic.csv'), columns=[CABIN, NAME, SURVIVED, EMBARKED])


# удаление пустых значений
def drop_values(data_set):
    return data_set[[CABIN, NAME, SURVIVED, EMBARKED]].dropna()


# удаление букв в значении кабины
def delete_cabin_letters(data_set):
    data_set[CABIN].update(data_set[CABIN].str.replace(r'\D', '', regex=True))
    data_set[CABIN] = pd.to_numeric(data_set[CABIN])
    return data_set


# замена значений буквенных значений Embarked
def replace_embarked_values(data_set):
    return data_set[[CABIN, NAME, SURVIVED]].join(data_set[EMBARKED].map({"C": 1, "S": 2, "Q": 3}))


# замена значений имени на числовые значения
def replace_name_to_number(word):
    result = 1
    for letter in word:
        result += ord(letter) + result % 10
    return result


dataF = get_dataframe()
dataF = replace_embarked_values(dataF)
dataF = delete_cabin_letters(dataF)
dataF = drop_values(dataF)
dataF[NAME] = dataF[NAME].apply(replace_name_to_number)


y = dataF[SURVIVED]  # выбор данных
x = dataF[[EMBARKED, CABIN, NAME]]  # выбор данных

decisionTreeClassifier = DecisionTreeClassifier() # классификатор дерева решений
decisionTreeClassifier.fit(x, y)  # построение дерева решений

print(decisionTreeClassifier.feature_names_in_)  # вывод названия признаков
print(decisionTreeClassifier.feature_importances_)  # вывод важности признаков
