import unittest

from pikov_lab_1_16.lab_1 import calculate_fare_sum_of_dead_passengers


class TestLab1(unittest.TestCase):

    def test_calculating_fair_sum_of_dead_passengers(self):
        calculated_sum = calculate_fare_sum_of_dead_passengers(
            data_filename='test_titanic.csv'
        )
        expected_sum = 6.6
        self.assertEqual(expected_sum, calculated_sum)
