import pandas as pd
from pandas import DataFrame


# количество схожих объектов
number_similar = 2


CANDY_NAME = 0
NAME_SIMILAR = 1
PERCENTAGE = 2


# считываем данные
def read_excel(filename):
    return pd.read_excel(filename , index_col=0)


# находим для каждого объекта похожие объекты
def similarity_data(df):
    rec = DataFrame()
    rec_list = list()

    for i in df:
        k = 0
        corr_matrix = df.corrwith(df[i])
        corr_matrix = DataFrame(corr_matrix)
        temp_matrix = corr_matrix
        temp_matrix = temp_matrix.drop([i], axis=0)
        while k != number_similar:
            name = temp_matrix.idxmax().item()
            value = temp_matrix[0][temp_matrix.idxmax().item()]
            rec_list.append([i, name, round(value, 2) * 100])
            temp_matrix = temp_matrix.drop([temp_matrix.idxmax().item()], axis=0)
            k += 1
    rec = rec.append(rec_list, ignore_index=True)
    return rec


# выводим объекты, которые похожи на запрашиваемый
def recommendation(candy_name, rec):
    print("По оценкам потребителей конфеты " + str(candy_name) + " схожи с: ")
    for i in range(len(rec)):
        if rec[CANDY_NAME][i] == candy_name:
            print(str(rec[NAME_SIMILAR][i]) + "(на " + str(rec[PERCENTAGE][i]) + "%)")


data = read_excel('DataSet.xlsx')
recommendations = similarity_data(data)

recommendation('Коркунов', recommendations)