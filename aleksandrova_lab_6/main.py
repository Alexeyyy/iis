import pandas as pd

PATH_DATASET = 'dataset.xlsx'
INDEX_COL = 'User'
COUNT_RECOMMEND = 2


def load_data(path):
    return pd.read_excel(path, index_col=INDEX_COL)


def create_recommend(ratings):
    recommendations = {}
    for row in ratings:
        recommendations[row] = []
        corr = ratings.corrwith(ratings[row])  # вычисление попарной корреляции
        del corr[row]  # удаляем из похожих товаров сам товар
        df_corr = pd.DataFrame(corr)
        for i in range(COUNT_RECOMMEND):
            product = df_corr.idxmax().item()
            value = df_corr[0][product]  # макс. значение попарной корреляции
            recommendations[row].append((product, value))
            df_corr = df_corr.drop([product], axis=0)  # удаляем объект с макс. значением корелляции
    return recommendations


def get_recommend(target_product, recommendations):
    print(f'Акварель "{target_product}" схожа с:')
    for i, product in enumerate(recommendations[target_product]):
        print(f'{i + 1}. {product[0]} на {int(product[1] * 100)}%')


ratings = load_data(PATH_DATASET)
recommendations = create_recommend(ratings)
get_recommend('Белые ночи', recommendations)
