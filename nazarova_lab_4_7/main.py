from sklearn.feature_selection import RFE
from sklearn.linear_model import Lasso
from sklearn.preprocessing import MinMaxScaler
import numpy as np


def regression_freedman_problem(size):
    X = np.random.uniform(0, 1, (size, 14))
    """
    Функция-выход регрессионной проблемы Фридмана
    """
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    """
    Зависимость признаков
    """
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y


"""
На вход: готовый список, список оценок по признакам
На выход: словарь из попарно сопоставленных элементов списков
"""

def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


"""
Вывод отсортированных по возрастанию попарно сопоставленных элементов списков
"""

def print_data(rank):
    for key in rank:
        rank[key] = {k: v for k, v in sorted(
            rank[key].items(), key=lambda item: item[1])}
    for key in rank:
        print(key)
        print(rank[key])


"""
Генерация исходных данных 
750 строк-наблюдений
14 столбцов-признаков
"""
np.random.seed(0)
size = 750
X, Y = regression_freedman_problem(size)

"""
Лассо (Lasso)
"""
lasso = Lasso(alpha=.001)
lasso.fit(X, Y)

"""
Случайное Лассо (RandomizedLasso)
"""
randomized_lasso = Lasso(random_state=0)
randomized_lasso.fit(X, Y)

"""
Рекурсивное сокращение признаков (Recursive Feature Elimination – RFE) 
"""
rfe = RFE(lasso)
rfe.fit(X, Y)


names = ["x%s" % i for i in range(1, 15)]
rank = {"Лассо:": rank_to_dict(lasso.coef_, names),
        "Случайное Лассо:": rank_to_dict(randomized_lasso.coef_, names),
        "Рекурсивное сокращение признаков:": rank_to_dict(rfe.support_, names)}

print_data(rank)
