import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors


# Чтение файлов
def read_file(file_name):
    return pd.read_csv(file_name, sep=';', encoding="latin-1", error_bad_lines=False)


def change_titles(books, users, ratings):
    books = books[['ISBN', 'Book-Title', 'Book-Author', 'Year-Of-Publication', 'Publisher']]
    books.rename(
        columns={'Book-Title': 'title', 'Book-Author': 'author', 'Year-Of-Publication': 'year',
                 'Publisher': 'publisher'},
        inplace=True)
    users.rename(columns={'User-ID': 'user_id', 'Location': 'location', 'Age': 'age'}, inplace=True)
    ratings.rename(columns={'User-ID': 'user_id', 'Book-Rating': 'rating'}, inplace=True)


def remove_ratings_user(amount, data_with_ratings):
    """removes all users which has amount reviews lower than amount"""
    x = data_with_ratings['user_id'].value_counts() > amount
    y = x[x].index  # айдишники пользователей
    # проверка есть ли рейтинг
    return data_with_ratings[data_with_ratings['user_id'].isin(y)]


def remove_ratings_book(ratings, books):
    # По ISBN мерджим книгу и рейтинг
    rating_with_books = ratings.merge(books, on='ISBN')

    number_rating = rating_with_books.groupby('title')['rating'].count().reset_index()
    number_rating.rename(columns={'rating': 'number_of_ratings'}, inplace=True)
    final_rating = rating_with_books.merge(number_rating, on='title')
    # вычленяем книги у которых
    final_rating = final_rating[final_rating['number_of_ratings'] >= 50]
    final_rating.drop_duplicates(['user_id', 'title'], inplace=True)
    return final_rating


def pivot_table(rating):
    """generates pivot table for data"""
    book_pivot = rating.pivot_table(columns='user_id', index='title', values="rating")
    book_pivot.fillna(0, inplace=True)
    return book_pivot


def generate_fit_model(book_pivot):
    book_sparse = csr_matrix(book_pivot)
    # Метод k-ближайших соседей
    model = NearestNeighbors(algorithm='brute')
    model.fit(book_sparse)
    return model


def main():
    books = read_file("BX-Books.csv")
    users = read_file("BX-Users.csv")
    ratings = read_file("BX-Book-Ratings.csv")
    # Заголовки
    change_titles(books, users, ratings)

    ratings = remove_ratings_user(200)
    final_rating = remove_ratings_book(ratings, books)

    book_pivot = pivot_table(final_rating)

    model = generate_fit_model(book_pivot)

    # Harry Potter
    print(book_pivot[237:238])

    distances, suggestions = model.kneighbors(book_pivot.iloc[237, :].values.reshape(1, -1))

    for i in range(len(suggestions)):
        print(book_pivot.index[suggestions[i]])


if __name__ == '__main__':
    main()
