import eli5 as eli5
import numpy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor


def readData():
    return pd.read_csv("titanic.csv")

PASSENGERID = "PassengerId"
SURVIVED = "Survived"
PCLASS = "Pclass"
NAME = "Name"
SEX = "Sex"
AGE = "Age"
SIBSP = "SibSp"
PARCH = "Parch"
TICKET = "Ticket"
FARE = "Fare"
CABIN = "Cabin"
EMBARKED = "Embarked"

def delNoNeedData(dataForDel):
    dataForDel = dataForDel.drop(PASSENGERID,axis=1)
    dataForDel = dataForDel.drop(PCLASS,axis=1)
    dataForDel = dataForDel.drop(NAME,axis=1)
    dataForDel = dataForDel.drop(SEX,axis=1)
    dataForDel = dataForDel.drop(TICKET,axis=1)
    dataForDel = dataForDel.drop(FARE,axis=1)
    dataForDel = dataForDel.drop(CABIN,axis=1)
    dataForDel = dataForDel.drop(EMBARKED,axis=1)
    return dataForDel



dataset = readData()
data = dataset.drop(SURVIVED,axis=1)
Survived = dataset[SURVIVED]
data = delNoNeedData(data)


def delNaN(data):
    data[AGE].update(data[AGE].replace(np.nan, data[AGE].median()))
    data[SIBSP].update(data[SIBSP].replace(np.nan, data[SIBSP].min()))
    data[PARCH].update(data[PARCH].replace(np.nan, data[PARCH].min()))
    return data


data = delNaN(data)
data_train, data_test, Survived_train, Survived_test = train_test_split(data, Survived, test_size=0.20)
regressor = DecisionTreeRegressor()
regressor.fit(data_train, Survived_train)
Survived_pred = regressor.predict(data_test)
df=pd.DataFrame({'Actual':Survived_test, 'Predicted':Survived_pred})
df


#print(data)
#print('Mean Absolute Error:', metrics.mean_absolute_error(Survived_test, Survived_pred))
#print('Mean Squared Error:', metrics.mean_squared_error(Survived_test, Survived_pred))
#print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(Survived_test, Survived_pred)))
print(regressor.feature_names_in_)
print(list(map(lambda x : round(x, 5), regressor.feature_importances_)))
