# 9 Pclass, Cabin, Embarked

import pandas as pd
from sklearn.tree import DecisionTreeClassifier


COL_PCLASS = 'Pclass'
COL_CABIN = 'Cabin'
COL_EMBARKED = 'Embarked'
COL_SURVIVED = 'Survived'


def load_file(path: str) -> pd.DataFrame:
    return pd.read_csv(path)


# Убираем пустые значения
def delete_empty():
    return dataF[[COL_PCLASS, COL_CABIN, COL_EMBARKED, COL_SURVIVED]].dropna()


# Убираем буквы в значении Cabin
def delete_letters():
    dataF[COL_CABIN].update(dataF[COL_CABIN].str.replace(r'\D', '', regex=True))
    dataF[COL_CABIN] = pd.to_numeric(dataF[COL_CABIN])
    return dataF


# Заменяем буквы на цифры в значении Embarked
def replace_values():
    return dataF[[COL_PCLASS, COL_CABIN]].join(dataF[COL_EMBARKED].map({"S": 1, "C": 2, "Q": 3}))


dataF = pd.DataFrame(data=load_file('titanic.csv'), columns=[COL_PCLASS, COL_CABIN, COL_EMBARKED, COL_SURVIVED])
dataF = delete_letters()
dataF = delete_empty()
# Выбираем данные
y = dataF[COL_SURVIVED]
x = replace_values()
dtc = DecisionTreeClassifier()
# Обучение модели
dtc.fit(x, y)
# вывод результатов
print(dtc.feature_names_in_)
print(dtc.feature_importances_)
