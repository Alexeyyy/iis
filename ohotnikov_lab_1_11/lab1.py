#11. Сколько женщин ехало в первом классе?
import pandas as pd

DATA_FILE_NAME = 'titanic.csv'
SEX_COL = 'Sex'
PCLASS_COL = 'Pclass'
SEX_VAL = 'female'
PCLASS_VAL = 1


def load_file(file):
    return pd.read_csv(file)


data = load_file(DATA_FILE_NAME)


def get_females_count():
     return data[(data[SEX_COL]==SEX_VAL) & (data[PCLASS_COL]==PCLASS_VAL)].shape[0]
    
print(str(get_females_count()) + ' ' + 'женщин плыло в первом классе')