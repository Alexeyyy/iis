from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import  Ridge, Lasso
from sklearn.preprocessing import MinMaxScaler
import numpy as np

def sort(data):
    sorted_values = sorted(data.values())
    sorted_data = {}
    for i in sorted_values:
        for j in data.keys():
            if data[j] == i:
                sorted_data[j] = data[j]
                break
    return sorted_data

def regression_problem(size=750):
    X = np.random.uniform(0, 1, (size, 14))
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y

def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))

def print_data(rank):
    for i in range(len(rank)):
        print(sort(rank[i]))


np.random.seed(0)
X, Y = regression_problem()

#гребневая модель
ridge = Ridge(alpha=7)
ridge.fit(X, Y)
#Случайное Лассо
lasso = Lasso(random_state=0)
lasso.fit(X, Y)
#Сокр. признаков случ. деревьями
regr_tree = RandomForestRegressor(random_state=0)
regr_tree.fit(X, Y)

names = ["x%s" % i for i in range(1,15)]
rank = [rank_to_dict(ridge.coef_, names),
        rank_to_dict(lasso.coef_, names),
        rank_to_dict(regr_tree.feature_importances_, names)]

print_data(rank)


