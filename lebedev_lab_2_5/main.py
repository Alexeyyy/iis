import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import numpy as np

'''
Вариант 5 SibSp,Parch,Ticket
'''

DATA_FILE_NAME = 'titanic.csv'
SIB_SP = "SibSp"
PARCH = "Parch"
TICKET = "Ticket"
SURVIVED = "Survived"


def load_file(file):
    return pd.read_csv(file)


def string_to_float(df, column):
    df[column] = pd.to_numeric(df[column], errors='coerce')
    df = df.replace(np.nan, 0, regex=True)
    return df


df = pd.DataFrame(data=load_file(DATA_FILE_NAME), columns=[SIB_SP, PARCH, TICKET, SURVIVED])
df = string_to_float(df, TICKET)
x = df[[SIB_SP, PARCH, TICKET]]
y = df[SURVIVED]
dtc = DecisionTreeClassifier()
dtc.fit(x, y)
for i in range(len(dtc.feature_importances_)):
    print(f"Признак {dtc.feature_names_in_[i]} важен на: {dtc.feature_importances_[i]}")
