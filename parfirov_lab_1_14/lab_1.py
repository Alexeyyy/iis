import pandas as pd

SURVIVED = 'Survived'
FARE = 'Fare'
FILE_NAME = 'titanic.csv'

def select_dead_passengers_from(passengers):
    is_survived = passengers[SURVIVED]
    dead_passengers_selection = passengers[is_survived == True]
    return dead_passengers_selection


def calculate_fare_sum_for(passengers):
    fair = passengers[FARE]
    fare_sum = fair.sum()
    return fare_sum

def calculate_fare_sum_of_dead_passengers(data_filename):
    passengers_table = pd.read_csv(data_filename)

    dead_passengers = select_dead_passengers_from(passengers_table)
    fare_sum_of_dead_passengers = calculate_fare_sum_for(dead_passengers)

    return fare_sum_of_dead_passengers

def main():
    dead_passengers_fare_sum = calculate_fare_sum_of_dead_passengers(
        data_filename = FILE_NAME
    )
    print('14. Какова суммарная стоимость билетов у выживших? Ответ: %s' % dead_passengers_fare_sum)

main()
