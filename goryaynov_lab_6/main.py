﻿import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel


def print_recommendations(name, top_limit):
    car_id = car_index[name]
    # получаем матрицу попарных сходств для запрошенного автомобиля
    cosine_scores = list(enumerate(cosine_similarity_matrix[car_id]))
    # ранжируем по убыванию меры сходства
    cosine_scores = sorted(cosine_scores, key=lambda x: x[1], reverse = True)
    top_scores = cosine_scores[0 : top_limit + 1]
    car_indexes = [i[0] for i in top_scores]
    car_scores = [i[1] for i in top_scores]
    recommended_cars_frame = pd.DataFrame(columns=["brand", "model", "score"])
    recommended_cars_frame["model"] = cars.loc[car_indexes, "model"]
    recommended_cars_frame["score"] = car_scores
    recommended_cars_frame["brand"] = cars.loc[car_indexes, "brand"]
    recommended_cars_frame.reset_index(inplace=True)  
    recommended_cars_frame.drop(["index"], axis=1, inplace=True)
    print(recommended_cars_frame)


cars = pd.read_csv("cars.csv")
# модель, оценивающая важность слов в контексте датасета
tfidf_vectorizer = TfidfVectorizer(stop_words="english")
cars['model'] = cars['model'].fillna(" ")
car_index = pd.Series(cars.index, index=cars['model']).drop_duplicates()
# нормализация исходной матрицы (сходство будем оценивать на основе предназначения автомобиля)
tfidf_matrix = tfidf_vectorizer.fit_transform(cars.purpose)
# в качестве показателя схожести автомобилей будем использовать косинусное сходство
cosine_similarity_matrix = linear_kernel(tfidf_matrix, tfidf_matrix)


print('---Dodge Challenger---')
print_recommendations("challenger",top_limit=7)
print('---GMC Vandura---')
print_recommendations("vandura",top_limit=7)