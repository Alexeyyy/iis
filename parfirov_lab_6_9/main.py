import pandas as pd

GENRES = 'genres'
RATING = 'rating'
ADVENTURE = 'Adventure'

ratings = pd.read_csv("ratings1.csv")
movies = pd.read_csv("movies1.csv")

sorted_ratings = ratings.sort_values(by='rating', ascending=False)
moviesTopIndex = []
arrayRatings = []

def search_movie_by_genre(genre):
    for index in range(1, len(movies.index)):
        strArrayMovie = movies.iloc[index][GENRES]
        array = strArrayMovie.split('|')
        for indexStrArray in range(0, len(array)):
            if array[indexStrArray] == genre:
                moviesTopIndex.append(movies.iloc[index])
        
def print_top_10_movies_by_genre():
    flag = 0
    for index, object_raiting in sorted_ratings.iterrows():
        for number in range(0, len(moviesTopIndex)):
            if int(object_raiting.movieId) == moviesTopIndex[number].movieId:
                flag += 1
                print(moviesTopIndex[number].title)
            if flag == 10:
                break
        if flag == 10:
                break

search_movie_by_genre(ADVENTURE)
print_top_10_movies_by_genre()
