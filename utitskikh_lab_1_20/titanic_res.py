import pandas as pd

data = pd.read_csv('titanic.csv');


def get_females():
    count = 0
    for _, row in data.iterrows():
        if str(row['Sex']) == 'female':
            if str(row['Pclass']) == '3':
                count += 1
    return count


print('Количество женщин в третьем классе' + ' ' + str(get_females()))
