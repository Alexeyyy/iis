import matplotlib.pyplot as plt
from sklearn.cluster import AgglomerativeClustering
from sklearn.datasets import make_moons


def get_dataset():
    return make_moons(noise=0.2, random_state=1)


def print_result(cl):
    print(cl.labels_)
    plt.scatter(X[:, 0], X[:, 1], c=cl.labels_, cmap='rainbow')
    plt.show()


def get_cluster():
    cluster = AgglomerativeClustering(n_clusters=2, affinity='euclidean', linkage='ward')
    cluster.fit_predict(X)
    return cluster


print(get_dataset()[0])
X = get_dataset()[0]
print_result(get_cluster())

