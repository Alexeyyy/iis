import pandas

#вариант1
#Какое количество мужчин и женщин ехало на корабле? В качестве ответа приведите два числа через пробел.


PATH_TITANIC = 'titanic.csv'
INDEX_COL = 'PassengerId'
SEX_COL = 'Sex'
SEX_MALE = 'male'
SEX_FEMALE = 'female'


def load_data(path, index_col):
    return pandas.read_csv(path, index_col=index_col)


def get_sex_counts(df, sex_col):
    return df[sex_col].value_counts()


if __name__ == '__main__':
    data = load_data(PATH_TITANIC, INDEX_COL)
    sex = get_sex_counts(data, SEX_COL)
    print(sex[SEX_MALE], sex[SEX_FEMALE])
