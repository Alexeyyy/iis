import pandas as pd
from pandas import DataFrame

# Сколько мужчин ехало в первом классе?


def get_data(filename) -> DataFrame:
    return pd.read_csv(filename)


def get_count_mens(data) -> int:
    count = 0
    for _, row in data.iterrows():
        if str(row['Pclass']) == '1' and str(row['Sex']) == 'male':
            count += 1
    return count


print(get_count_mens(get_data('titanic.csv')))
