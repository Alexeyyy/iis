import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import PolynomialFeatures


class Models:

    def linear(self, X_train, X_test, y_train, y_test):
        model = LinearRegression().fit(X_train, y_train)
        y_pred = model.predict(X_test)

        print('___Линейная регрессия___')
        print('Линейный коэффициент детерминации:', model.score(X_train, y_train))
        print('')

        plt.scatter(X_test, y_test, color='black')
        plt.plot(X_test, y_pred, color='blue', linewidth=3)
        plt.show()

    def polynomial(self, X, y):
        poly_reg = PolynomialFeatures(degree=3)
        X_poly = poly_reg.fit_transform(X)
        pol_reg = LinearRegression()
        pol_reg.fit(X_poly, y)
        y_pred = pol_reg.predict(X_poly)

        print('___Полиномиальная  регрессия___')
        print('Полиномиальный коэффициент детерминации:', pol_reg.score(X_poly, y))
        print('')

        plt.scatter(X, y, color='red')
        plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
        plt.show()

    def multi_layer_perceptron(self, X_TRAIN, Y_TRAIN, X_TEST):
        model = MLPClassifier(hidden_layer_sizes=1000, alpha=0.01)
        model.fit(X_TRAIN, Y_TRAIN)

        print("Средняя точность: ",model.score(X_TRAIN, Y_TRAIN))
        plt.scatter(X_TRAIN, Y_TRAIN, color='blue')
        plt.scatter(X_TRAIN, model.predict(X_TEST), color='red')
        plt.show()
