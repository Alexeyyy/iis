import numpy as np
from sklearn.datasets import make_circles
from models import Models

models_manager = Models()

data = make_circles(noise=0.2, factor=0.5, random_state=1)

X = np.array(data[0])
Y = np.array(data[1])
X = X[:, np.newaxis, 1]

X_train = X[:-45]
X_test = X[45:]
y_train = Y[:-45]
y_test = Y[45:]

models_manager.linear(X_train, X_test, y_train, y_test)

models_manager.polynomial(X_train, y_train)

models_manager.multi_layer_perceptron(X_train, y_train, X_test)
