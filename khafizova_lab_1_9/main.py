# 9.Найдите имя самого пожилого человека.

import pandas as pd

load_data = 'titanic.csv'
COL_AGE = 'Age'
COL_NAME = 'Name'


def search_max_age_person(data) -> str:
    data = pd.read_csv(data, delimiter=',')
    person_id = 0  # задаем индекс для самого пожилого человека
    for row in range(0, data[COL_AGE].count()-1):  # проходимся по всем пассажирам
        if data[COL_AGE][row] == data.Age.max():  # сравниваем, возраст пассажира и возраст самого пожилого человека
            person_id = row  # находим индекс самого пожилого человека
            continue
    return "Имя самого пожилого человека: " + str(data[COL_NAME][person_id]) + " Возраст самого пожилого человека: " \
           + str(int(data.Age.max()))


print(search_max_age_person(load_data))
