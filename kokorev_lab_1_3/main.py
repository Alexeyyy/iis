"""3. Какую долю пассажиры первого класса составляли среди всех пассажиров?

Ответ приведите в процентах (число в интервале от 0 до 100, знак процента не нужен),
округлив до двух знаков.
"""

import pandas
from pandas import DataFrame


def load_data(path: str) -> DataFrame:
    """чтение таблицы из файла"""
    return pandas.read_csv(path)


def get_percent(part: int, total: int) -> float:
    """подсчет процента"""
    percent = part / total * 100
    return round(percent, 2)  # округляем процент до двух знаков


data: DataFrame = load_data('titanic.csv')

first_class_count = 0
total_count = 0
for _, row in data.iterrows():  # идем по строкам из файла
    total_count += 1  # увеличиваем общее число пассажиров на каждом цикле
    if str(row['Pclass']) == '1':  # если пассажир в первом классе (в стобце Pclass указана 1ца)
        first_class_count += 1  # увеличиваем число пассажиров первого класса

percentage = get_percent(first_class_count, total_count)
print(percentage)
