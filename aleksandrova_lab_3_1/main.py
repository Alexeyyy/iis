import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
from sklearn.datasets import make_moons
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures


def linear(X_train, X_test, y_train, y_test):
    model = LinearRegression().fit(X_train, y_train)
    y_predict = model.predict(X_test)
    plt.title('Линейная регрессия')
    plt.scatter(X_test, y_test, color='black')
    plt.plot(X_test, y_predict, color='red')
    plt.show()
    print('Линейная регрессия')
    print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_test, y_predict))
    print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_test, y_predict))


def polynomial(X_train, y_train):
    polynomial_features = PolynomialFeatures(degree=3)  # коэффициенты полинома
    X_polynomial = polynomial_features.fit_transform(X_train)
    base_model = LinearRegression()
    base_model.fit(X_polynomial, y_train)
    y_predict = base_model.predict(X_polynomial)
    plt.title('Полиномиальная регрессия')
    plt.scatter(X_train, y_train, color='black')
    plt.plot(X_train, y_predict, color='red')
    plt.show()
    print('Полиномиальная регрессия:')
    print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_train, y_predict))
    print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_train, y_predict))


def ridge(X_train, X_test, y_train, y_test):
    model = Pipeline([('poly', PolynomialFeatures(degree=3)), ('ridge', Ridge(alpha=1.0))])
    model.fit(X_train, y_train)
    y_predict = model.predict(X_test)
    plt.title('Гребневая полиномиальная регрессия')
    plt.scatter(X_test, y_test, color='black')
    plt.plot(X_test, y_predict, color='red')
    plt.show()
    print('Гребневая полиномиальная регрессия:')
    print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_test, y_predict))
    print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_test, y_predict))


moon_dataset = make_moons(noise=0.3, random_state=0)
X, y = moon_dataset
X = X[:, np.newaxis, 1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)
linear(X_train, X_test, y_train, y_test)
polynomial(X_train, y_train)
ridge(X_train, X_test, y_train, y_test)
