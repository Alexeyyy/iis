from scipy.cluster.hierarchy import linkage, dendrogram
import matplotlib.pyplot as plt
import pandas as pd

seeds_df = pd.read_csv("dota 2 hero synergy.csv")

varieties = list(seeds_df.pop('hero1_id'))
varieties = list(seeds_df.pop('hero2_id'))

samples = seeds_df.values

mergings = linkage(samples, method='complete')

dendrogram(mergings,
           labels=varieties,
           leaf_rotation=90,
           leaf_font_size=6,
           )

plt.show()