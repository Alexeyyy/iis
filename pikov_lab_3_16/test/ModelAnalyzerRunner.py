import unittest

from sklearn.datasets import make_moons
from sklearn.linear_model import LinearRegression, Perceptron
from sklearn.neural_network import MLPClassifier

from pikov_lab_3_16.ModelAnalyzer import ModelAnalyzer


# 16.  Данные: make_moons (noise=0.3, random_state=rs)
# Модели:
# · Линейную регрессию
# · Многослойный персептрон с 10-ю нейронами в скрытом слое (alpha = 0.01)
# · Персептрон
class ModelAnalyzerRunner(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        data = make_moons(noise=0.3, random_state=16)
        cls._model_analyzer = ModelAnalyzer(data)

    def test_linear_regression(self):
        print("\nМодель - Линейная регрессия:")
        model = LinearRegression()
        self._model_analyzer.analyze(model)

    def test_perceptron(self):
        print("\nМодель - Перцептрон:")
        model = Perceptron()
        self._model_analyzer.analyze(model)

    def test_multi_layer_perceptron(self):
        print("\nМодель - Многослойный перцептрон:")
        model = MLPClassifier(hidden_layer_sizes=10, alpha=0.01)
        self._model_analyzer.analyze(model)


if __name__ == '__main__':
    unittest.main()
