import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot


def get_data(filename) -> DataFrame:
    return pd.read_csv(filename)


def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)
    tree.plot_tree(classifier)
    pyplot.show()

def name_to_number(word):
    result = 1
    for letter in word:
        result += ord(letter) + result % 10
    return result


def get_dataframe():
    return pd.DataFrame(data=get_data('titanic.csv'), columns=['Name', 'Fare', 'Embarked', 'Survived'])


def delete_none():
    return dataframe[['Name', 'Fare', 'Embarked', 'Survived']].dropna()


dataframe = get_dataframe()
dataframe = delete_none()
dataframe['Name'] = dataframe['Name'].apply(name_to_number)

classifier = DecisionTreeClassifier()

y = dataframe['Survived']
x = dataframe[['Name', 'Fare']].join(dataframe['Embarked'].map({"S": 3, "C": 1, "Q": 2}))

classifier.fit(x, y)

print_results(classifier)
