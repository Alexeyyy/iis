import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot


def load_file(file) -> DataFrame:
    return pd.read_csv(file)


def get_dataframe():
    return pd.DataFrame(data=load_file('titanic.csv'), columns=['Cabin', 'Age', 'Embarked', 'Survived'])


def delete_none():
    return DataFrame[['Cabin', 'Age', 'Embarked', 'Survived']].dropna()


def delete_let(strCabin):
    res = ""
    for i in strCabin:
        if i.isdigit():
            res +=i
    return res


def set_cabin_num(DataFrame, strCab):
    for i in range(len(DataFrame[strCab])):
        DataFrame[strCab].update(DataFrame[strCab].replace(DataFrame[strCab][i],
                                                           delete_let(str(DataFrame[strCab][i]))))
    DataFrame[strCab] = pd.to_numeric(DataFrame[strCab])
    return DataFrame


def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)
    tree.plot_tree(classifier)
    fig = pyplot.gcf()
    fig.set_size_inches(9, 6)
    pyplot.show()


DataFrame = get_dataframe()
DataFrame = set_cabin_num(DataFrame, 'Cabin')
DataFrame = delete_none()


y = DataFrame['Survived']
x = DataFrame[['Cabin', 'Age']].join(DataFrame["Embarked"].map({"S": 1, "C": 2, "Q": 3}))
classifier = DecisionTreeClassifier()
classifier.fit(x, y)
print_results(classifier)

