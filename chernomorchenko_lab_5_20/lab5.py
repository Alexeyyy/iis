import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.datasets import make_moons


X , y = make_moons(n_samples = 1000, noise = 0.1)
print(X)
plt.scatter(X[:, 0], X[:, 1], c = "orange", label = 'Исходные данные')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 0)
plt.show()

dbscan_centers = DBSCAN(eps = 0.1, min_samples = 10).fit(X)
names = dbscan_centers.labels_
plt.scatter(X[:, 0], X[:, 1], c = names, label ='DBSCAN eps = 0.1, min_samples = 10')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 0)
plt.show()