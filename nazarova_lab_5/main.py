import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import AgglomerativeClustering

"""
Иерархическая кластеризация
"""
def clustering():
    cluster = AgglomerativeClustering(
        n_clusters=2, affinity='euclidean', linkage='ward')
    cluster.fit_predict(X)
    return cluster


"""
Вывод результата
"""
def result(cl):
    print(cl.labels_)
    plt.scatter(X[:, 0], X[:, 1], c=cl.labels_, cmap='brg')
    plt.show()


"""
Генерация массива точек
"""
X = np.array([[13, 15], [15, 20], [20, 17], [30, 15], [25, 25], 
              [75, 80], [71, 80], [68, 70], [75, 65], [80, 91], ])
result(clustering())
