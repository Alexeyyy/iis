import numpy as np
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.preprocessing import MinMaxScaler


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def sort_data(dict1):
    sorted_values = sorted(dict1.values())
    sorted_dict = {}

    for i in sorted_values:
        for k in dict1.keys():
            if dict1[k] == i:
                sorted_dict[k] = dict1[k]
                break
    return sorted_dict


np.random.seed(0)
size = 750
X = np.random.uniform(0, 1, (size, 14))

print(X)

# Задаем функцию-выход: регрессионную проблему Фридмана
Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
     10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))

# Добавляем зависимость признаков
X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
lr = LinearRegression()
lr.fit(X, Y)

rd = Ridge()
rd.fit(X, Y)

ls = Lasso()
ls.fit(X, Y)

rls = Lasso(random_state=0)
rls.fit(X, Y)


names = ["x%s" % i for i in range(1, 15)]
rank = [rank_to_dict(lr.coef_, names),
        rank_to_dict(rd.coef_, names),
        rank_to_dict(ls.coef_, names),
        rank_to_dict(rls.coef_, names)]
for i in range(len(rank)):
    print(sort_data(rank[i]))
