import pandas as pd
from pandas import DataFrame


number_similar = 3


COFFEE_NAME = 0
NAME_SIMILAR = 1
PERCENTAGE = 2


def read_excel(filename):
    return pd.read_excel(filename, index_col=0)


def similarity_data(df, i):
    """рассчет схожести объектов"""
    rec_list = list()
    k = 0
    corr_matrix = df.corrwith(df[i])
    corr_matrix = DataFrame(corr_matrix)
    temp_matrix = corr_matrix
    temp_matrix = temp_matrix.drop([i], axis=0)
    while k != number_similar:
        name = temp_matrix.idxmax().item()
        value = temp_matrix[0][temp_matrix.idxmax().item()]
        rec_list.append([i, name, round(value, 2) * 100])
        temp_matrix = temp_matrix.drop(
            [temp_matrix.idxmax().item()], axis=0)
        k += 1
    return rec_list


def recommendation(coffee_name, rec):
    """вывод схожих объектов"""
    print("Топ 3 кофе подобных " + str(coffee_name))
    for i in range(len(rec)):
        if rec[COFFEE_NAME][i] == coffee_name:
            print(str(rec[NAME_SIMILAR][i]) +
                  " - схожесть " + str(rec[PERCENTAGE][i]) + "%")


data = read_excel('Data.xlsx')
rec = DataFrame()
for i in data:
    rec = rec.append(similarity_data(data, i), ignore_index=True)
recommendations = rec
recommendation('Bushido', recommendations)
