# 10.Линейная регрессия (LinearRegression), Лассо (Lasso),Рекурсивное сокращение признаков (Recursive Feature
# Elimination – RFE)

from sklearn.linear_model import Lasso, LinearRegression
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from sklearn.feature_selection import f_regression, RFE


def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def sort_data(dict_ranks):
    sorted_values = sorted(dict_ranks.values())
    sorted_dict = {}
    for i in sorted_values:
        for k in dict_ranks.keys():
            if dict_ranks[k] == i:
                sorted_dict[k] = dict_ranks[k]
                break
    return sorted_dict


# генерируем исходные данные:
# 750 строк-наблюдений и 14 столбцов-признаков
np.random.seed(1)
size = 750
X = np.random.uniform(0, 1, (size, 14))
# регрессионная проблема Фридмана
Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 + 10 * X[:, 3] +
     5 * X[:, 4] ** 5 + np.random.normal(0, 1))
# Зависимость признаков
X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))

print('X')
print(X)
print('Y')
print(Y)
# Линейная регрессия
linear_regression = LinearRegression()
linear_regression.fit(X, Y)
# Лассо
lasso = Lasso(alpha=.001)
lasso.fit(X, Y)
# Рекурсивное сокращение признаков
rfe = RFE(linear_regression)
rfe.fit(X, Y)

names = ["x%s" % i for i in range(1, 15)]
ranks = {"Lasso": rank_to_dict(lasso.coef_, names),
         "Linear Regression": rank_to_dict(linear_regression.coef_, names),
         "RFE": rank_to_dict(rfe.support_, names)}

for key in ranks:
    ranks[key] = {k: v for k, v in sorted(ranks[key].items(), key=lambda item: item[1])}
for key in ranks:
    print(key)
    print(ranks[key])

