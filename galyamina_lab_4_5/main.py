from typing import Union
from sklearn.feature_selection import RFE
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Ridge
from sklearn.preprocessing import MinMaxScaler
import numpy as np


size = 750
X: Union
Y: Union

def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def create_data():
    global X, Y
    X = np.random.uniform(0, 1, (size, 14))
    # Задаем функцию для выхода: регрессионная проблема Фридмана
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))


# Добавим признакам зависимости
def add_dependency():
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))


def find_regs():
    # Гребневая регрессия
    ringe = Ridge()
    ringe.fit(X, Y)
    # Рекурсивное сокращение признаков
    rfe = RFE(ringe)
    rfe.fit(X, Y)
    # Сокращение признаков случайными деревьями
    tree = RandomForestRegressor(random_state=0)
    tree.fit(X, Y)
    names = ["name%s" % i for i in range(1, 15)]
    print("Гребневая регрессия:")
    print(rank_to_dict(ringe.coef_, names))
    print("Рекурсивное сокращение признаков:")
    print(rank_to_dict(rfe.support_, names))
    print("Сокращение признаков случайными деревьями:")
    print(rank_to_dict(tree.feature_importances_, names))


create_data()
add_dependency()
find_regs()