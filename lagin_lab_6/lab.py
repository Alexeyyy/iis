# Рекомендательная система фильмов на основе оценок пользователей

import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from scipy.sparse import csr_matrix

ratings = pd.read_csv("ratings.csv")
ratings.head()

movies = pd.read_csv("movies.csv")
movies.head()

n_ratings = len(ratings)
n_movies = len(ratings['movieId'].unique())
n_users = len(ratings['userId'].unique())

print(f"Количество оценок: {n_ratings}")
print(f"Количество уникальных id фильмов: {n_movies}")
print(f"Количество пользователей: {n_users}")
print(f"Средняя оценка пользователя: {round(n_ratings / n_users, 2)}")
print(f"Средняя оценка фильма: {round(n_ratings / n_movies, 2)}")

user_freq = ratings[['userId', 'movieId']].groupby('userId').count().reset_index()
user_freq.columns = ['userId', 'n_ratings']
user_freq.head()

# Найдем фильмы с минимальным и максимальным рейтингом:
mean_rating = ratings.groupby('movieId')[['rating']].mean()

# Фильм с минимальным рейтингом
lowest_rated = mean_rating['rating'].idxmin()
print("Фильм с минимальным рейтингом:")
print(movies.loc[movies['movieId'] == lowest_rated])

# Фильм с максимальным рейтингом
highest_rated = mean_rating['rating'].idxmax()
print("Фильм с максимальным рейтингом:")
print(movies.loc[movies['movieId'] == highest_rated])

# Отобразим количество людей, кто дал максимальную оценку фильмам
print("Люди, которые дали максимальную оценку фильмам:")
print(ratings[ratings['movieId'] == highest_rated])

# Отобразим количество людей, кто дал минимальную оценку фильмам
print("Люди, которые дали минимальную оценку фильмам:")
print(ratings[ratings['movieId'] == lowest_rated])

# У вышеперечисленных фильмов очень низкий набор данных. Мы будем использовать байесовское среднее
movie_stats = ratings.groupby('movieId')[['rating']].agg(['count', 'mean'])
movie_stats.columns = movie_stats.columns.droplevel()


def create_matrix(df):
    N = len(df['userId'].unique())
    M = len(df['movieId'].unique())

    # Мапаем Id на индексы
    user_mapper = dict(zip(np.unique(df["userId"]), list(range(N))))
    movie_mapper = dict(zip(np.unique(df["movieId"]), list(range(M))))

    # Мапаем индексы на Id
    user_inv_mapper = dict(zip(list(range(N)), np.unique(df["userId"])))
    movie_inv_mapper = dict(zip(list(range(M)), np.unique(df["movieId"])))

    user_index = [user_mapper[i] for i in df['userId']]
    movie_index = [movie_mapper[i] for i in df['movieId']]

    X = csr_matrix((df["rating"], (movie_index, user_index)), shape=(M, N))

    return X, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper


X, user_mapper, movie_mapper, user_inv_mapper, movie_inv_mapper = create_matrix(ratings)

"""
Ищем похожие фильмы используя модель KNN
"""

def find_similar_movies(movie_id, X, k, metric='cosine', show_distance=False):
    neighbour_ids = []

    movie_ind = movie_mapper[movie_id]
    movie_vec = X[movie_ind]
    k += 1
    kNN = NearestNeighbors(n_neighbors=k, algorithm="brute", metric=metric)
    kNN.fit(X)
    movie_vec = movie_vec.reshape(1, -1)
    neighbour = kNN.kneighbors(movie_vec, return_distance=show_distance)
    for i in range(0, k):
        n = neighbour.item(i)
        neighbour_ids.append(movie_inv_mapper[n])
    neighbour_ids.pop(0)
    return neighbour_ids


movie_titles = dict(zip(movies['movieId'], movies['title']))

# Выберем фильм для которого будем искать похожие
movie_id = 3

similar_ids = find_similar_movies(movie_id, X, k=10)
movie_title = movie_titles[movie_id]

print(f"Найденные фильмы на подобие просмотренного: {movie_title}")
for i in similar_ids:
    print(movie_titles[i])
