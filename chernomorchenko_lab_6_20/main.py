import pandas as pd
from sklearn.feature_extraction.text import \
    TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

NAME_CONST = "name"
RES_CONST = "similarity"
IND_CONST = "index"
FILE_CONST = "fooddata.csv"
LANG_CONST = "english"
SEARCH_FOOD = "Palathalikalu"

def get_food_advise(food_idx, food_sim_points):
    food_similar_dishes = pd.DataFrame(columns = [NAME_CONST, RES_CONST])
    food_similar_dishes[NAME_CONST] = food.loc[food_idx, NAME_CONST]
    food_similar_dishes[RES_CONST] = food_sim_points
    food_similar_dishes.reset_index(inplace = True)
    food_similar_dishes.drop([IND_CONST], axis = 1, inplace=True)
    print(food_similar_dishes)

def get_food_data(food_name, count):
    food_id = food_index[food_name]
    similar_scores = list(enumerate(similar_matrix[food_id]))
    similar_scores = sorted(similar_scores, key = lambda x: x[1], reverse = True)
    similar_scores = similar_scores[0:count + 1]
    food_idx = [i[0] for i in similar_scores]
    food_sim_points = [i[1] for i in similar_scores]
    get_food_advise(food_idx, food_sim_points)


food = pd.read_csv(FILE_CONST, sep = ';')
vect = TfidfVectorizer(stop_words = LANG_CONST)

vect_matrix = vect.fit_transform(food.ingredients)
similar_matrix = linear_kernel(vect_matrix, vect_matrix)
food_index = pd.Series(food.index, index = food[NAME_CONST]).drop_duplicates()

get_food_data(SEARCH_FOOD, count = 10)