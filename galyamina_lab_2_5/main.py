from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import re

# Найти у выживших пассажиров 2 наиболее важных признака из SibSp,Parch,Ticket
# Импорт набора данных
dataset = pd.read_csv('titanic.csv')

#проверка, что значение можно преобразовать в float
isfloat = re.compile(r"^[-+]?(?:\b[0-9]+(?:\.[0-9]*)?|\.[0-9]+\b)(?:[eE][-+]?[0-9]+\b)?$").match
def is_float_re(str):
    return True if isfloat(str) else False

# Так как в некоторых случаях поле Ticket кроме номера содержит дополнительные символы, нужно оставить только номер
# Если значение нечисловое, то заменим его на 0

def replace_data():
    for _, row in dataset.iterrows():
        if str(row['Ticket']).find(' ') != -1:
            index = len(str(row['Ticket']).split(' '))
            dataset['Ticket'] = dataset['Ticket'].replace(row['Ticket'], str(row['Ticket']).split(' ')[index-1])
        if(isfloat(row['Ticket'])):
            float(row['Ticket'])
        else:
            dataset['Ticket'] = dataset['Ticket'].replace(row['Ticket'], 0)


def build_tree():
    model = tree.DecisionTreeClassifier(criterion="entropy")
    model.max_depth = 3
    model.fit(X, Y)
    # Вывод признаков в порядке убывания важности
    print(model.feature_importances_)
    print(model.feature_names_in_)
    tree.plot_tree(model)
    plt.show()

replace_data()
X = dataset[['SibSp', 'Parch', 'Ticket']]
Y = dataset['Survived']
build_tree()
