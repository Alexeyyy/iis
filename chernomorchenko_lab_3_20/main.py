import numpy as np
from sklearn.datasets import make_circles
from serviceMethods import ServiceMethods

def prepare_data(X, Y):
    X_learn = X[:75]
    X_test = X[75:]
    Y_learn = Y[:75]
    Y_test = Y[75:]
    return X_learn, X_test, Y_learn, Y_test

service_methods = ServiceMethods()

data = make_circles(noise=0.2, factor=0.5, random_state=1)

X = np.array(data[0])
Y = np.array(data[1])
X =  np.delete(X, 0, 1)

X_learn, X_test, Y_learn, Y_test = prepare_data(X, Y)

service_methods.linear_regression(X_learn, X_test, Y_learn, Y_test)

service_methods.polynomial_regression(X_learn, Y_learn)

service_methods.ridge_regression(X_learn, X_test, Y_learn, Y_test)

