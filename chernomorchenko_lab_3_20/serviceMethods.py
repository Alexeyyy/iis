from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures
from errorMetrics import ErrorMetrics


class ServiceMethods:

    def linear_regression(self, X_learn, X_test, Y_learn, Y_test):
        regr = LinearRegression().fit(X_learn, Y_learn)
        y_pred = regr.predict(X_test)
        type = 'Линейная регрессия'
        ErrorMetrics.print_errors(Y_test, y_pred, type)

        plt.scatter(X_test, Y_test, color='royalblue')
        plt.plot(X_test, y_pred, color='coral', linewidth=2)
        plt.show()

    def polynomial_regression(self, X, Y):
        poly_reg = PolynomialFeatures(degree=5)
        X_poly = poly_reg.fit_transform(X)
        pol_reg = LinearRegression()
        pol_reg.fit(X_poly, Y)
        y_pred = pol_reg.predict(X_poly)

        type = 'Полиномиальная регрессия'
        ErrorMetrics.print_errors(Y, y_pred, type)

        plt.scatter(X, Y, color='royalblue')
        plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='plum')
        plt.show()

    def ridge_regression(self, X_learn, X_test, Y_learn, Y_test):
        clf = Ridge(alpha=1.0)
        clf.fit(X_learn, Y_learn)
        y_pred = clf.predict(X_test)

        type = 'Гребневая полиномиальная регрессия'
        ErrorMetrics.print_errors(Y_test, y_pred, type)

        plt.scatter(X_test, Y_test, color='royalblue')
        plt.plot(X_test, y_pred, color='coral', linewidth=2)
        plt.show()


