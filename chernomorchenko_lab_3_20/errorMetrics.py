import numpy as np

from sklearn import metrics

class ErrorMetrics:

    @staticmethod
    def print_errors(y_test, y_pred, type):
        print(str(type))
        print('Средняя абсолютная ошибка:', round((metrics.mean_absolute_error(y_test, y_pred)), 5))
        print('Среднеквадратичная ошибка:', round((metrics.mean_squared_error(y_test, y_pred)), 5))
        print('Среднеквадратичное отклонение:', round((np.sqrt(metrics.mean_squared_error(y_test, y_pred))), 5))
        print('')

