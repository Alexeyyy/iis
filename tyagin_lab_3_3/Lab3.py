import numpy as np
from sklearn.datasets import make_classification
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn import metrics

COUNT = -30


def linear_regression(x_train, x_test, y_train, y_test):
    lin_reg = LinearRegression().fit(x_train, y_train)
    y_pred = lin_reg.predict(x_test)
    print('Линейная регрессия')
    print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_test, y_pred))
    print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_test, y_pred))

    plt.title("Линейная регрессия")
    plt.scatter(x_test, y_test, color='red')
    plt.plot(x_test, y_pred, color='green', linewidth=3)
    plt.show()


def polynomial_regression(X, y):
    poly_reg = PolynomialFeatures(degree=3)
    X_poly = poly_reg.fit_transform(X)
    pol_reg = LinearRegression()
    pol_reg.fit(X_poly, y)
    y_pred = pol_reg.predict(X_poly)

    print('Полиномиальная регрессия')
    print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y, y_pred))
    print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y, y_pred))

    plt.title("Полиномиальная регрессия")

    plt.scatter(X, y, color='blue')
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='black')
    plt.show()


def ridge_regression(x_train, x_test, y_train, y_test):
    clf = Ridge(alpha=1.0)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)
    print('Гребневая полиномиальная регрессия')
    print('Средняя абсолютная ошибка:', metrics.mean_absolute_error(y_test, y_pred))
    print('Среднеквадратичная ошибка:', metrics.mean_squared_error(y_test, y_pred))

    plt.title("Гребневая полиномиальная регрессия")
    plt.scatter(x_test, y_test, color='red')
    plt.plot(x_test, y_pred, color='blue', linewidth=2)
    plt.show()


def get_data(x, y):
    x_train = x[:COUNT]
    x_test = x[COUNT:]
    y_train = y[:COUNT]
    y_test = y[COUNT:]
    return x_train, x_test, y_train, y_test


dataset = make_classification(n_samples=500, n_features=2, n_redundant=0, n_informative=2, random_state=1,
                              n_clusters_per_class=1)

print(dataset)

x = np.array(dataset[0])
y = np.array(dataset[1])

x = x[:, np.newaxis, 1]

x_train, x_test, y_train, y_test = get_data(x, y)

linear_regression(x_train, x_test, y_train, y_test)
polynomial_regression(x_train, y_train)
ridge_regression(x_train, x_test, y_train, y_test)
