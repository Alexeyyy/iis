"""4. Ранжирование признаков

Используя код из пункта «Решение задачи ранжирования признаков»,
выполните ранжирование признаков с помощью указанных по варианту.
Отобразите получившиеся значения / оценки каждого признака каждым методом / моделью и среднюю оценку.
Проведите анализ получившихся результатов. Какие 4 признака оказались самыми важными по среднему значению?
(Названия / индексы признаков и будут ответом на задание).

3. Линейная регрессия (LinearRegression), Сокращение признаков
Случайными деревьями (Random Forest Regressor), Линейная корреляция (f_regression)
"""

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import f_regression


def create_dataset():
    """генерация датасета из равномерно распределенных данных"""
    np.random.seed(0)
    size = 750
    X = np.random.uniform(0, 1, (size, 14))
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y


def convert_to_dict(ranks, names):
    """словарь из попарно сопоставленных элементов списка"""
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def get_mean(ranks):
    mean = {}
    for _, value in ranks.items():
        for item in value:
            if item not in mean:
                mean[item] = 0
            mean[item] += value[item]
    for value in mean:
        res = mean[value] / len(ranks)
        mean[value] = round(res, 2)
    return mean


X, Y = create_dataset()

linear_regression = LinearRegression()
linear_regression.fit(X, Y)

random_forest_regressor = RandomForestRegressor()
random_forest_regressor.fit(X, Y)
random_forest_result = random_forest_regressor.predict(X)[0:14]

f_regression = f_regression(X, Y)
f_regression = f_regression[0]

names = ["x%s" % i for i in range(1, 15)]

ranks = {"linear regression": convert_to_dict(linear_regression.coef_, names),
         "random forest regressor": convert_to_dict(random_forest_result, names),
         "f_regression": convert_to_dict(f_regression, names)}

mean = get_mean(ranks)
mean = {k: v for k, v in sorted(mean.items(), key=lambda item: item[1])}
print(f"MEAN: {mean}")
for key in ranks:
    ranks[key] = {k: v for k, v in sorted(ranks[key].items(), key=lambda item: item[1])}
for key in ranks:
    print(key)
    print(ranks[key])
