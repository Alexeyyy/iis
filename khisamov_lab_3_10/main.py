import numpy as np
from sklearn.datasets import make_moons
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPClassifier


def linear(x_train, y_train, x_test, y_test):
    model = LinearRegression().fit(x_train, y_train)
    y_pred = model.predict(x_test)
    print('Средняя точность:', model.score(x_train, y_train))
    plt.title("График линейной регрессии")
    print('')
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, y_pred, color='blue', linewidth=3)
    plt.show()


def do_perceptron100(x_train, y_train, x_test):
    perceptron = MLPClassifier(hidden_layer_sizes=100, alpha=0.01)
    perceptron.fit(x_train, y_train.ravel())
    print('Средняя точность:', perceptron.score(x_train, y_train))
    plt.title("График персептрона 100")
    print('')
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, perceptron.predict(x_test), color='red', linewidth=3)
    plt.show()


def do_perceptron10(x_train, y_train, x_test):
    perceptron = MLPClassifier(hidden_layer_sizes=10, alpha=0.01)
    perceptron.fit(x_train, y_train.ravel())
    print('Средняя точность:', perceptron.score(x_train, y_train))
    plt.title("График персептрона 10")
    print('')
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, perceptron.predict(x_test), color='red', linewidth=3)
    plt.show()


dataset = make_moons(noise=0.3, random_state=1)


X = np.array(dataset[0])
y = np.array(dataset[1])

X = X.reshape(-1, 1)
y = y.reshape(-1, 1)

x_train = X[:70]
x_test = X[-70:]
y_train = y[:70]
y_test = y[-70:]

linear(x_train, y_train, x_test, y_test)

do_perceptron100(x_train, y_train, x_test)

do_perceptron10(x_train, y_train, x_test)