from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA


# Загружаем данные
def get_dataset():
    return load_iris()


# Определяем модель и обучаем
def dbscan_clustering(iris):
    dbscan = DBSCAN()
    dbscan.fit(iris.data)
    return dbscan


# Уменьшаем размерность при помощи pca
def pca_method(iris):
    pca = PCA(n_components=2).fit(iris.data)
    pca_2d = pca.transform(iris.data)

    return pca_2d


# Строим график в соответствии с тремя классами
def print_result(pca_2d, dbscan):
    for i in range(0, pca_2d.shape[0]):
        if dbscan.labels_[i] == 0:
            c1 = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='mediumorchid')
        elif dbscan.labels_[i] == 1:
            c2 = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='lightsalmon')
        elif dbscan.labels_[i] == -1:
            c3 = plt.scatter(pca_2d[i, 0], pca_2d[i, 1], c='mediumaquamarine')

    plt.legend([c1, c2, c3], ['Кластер 1', 'Кластер 2', 'Шум'])
    plt.show()


data_iris = get_dataset()
dbscan_ = dbscan_clustering(data_iris)
pca_ = pca_method(data_iris)
print_result(pca_, dbscan_)



