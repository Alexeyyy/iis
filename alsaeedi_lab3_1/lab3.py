import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_circles
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline

circles_dataset = make_circles(noise=0.2, factor=0.5, random_state=1)

X, y = circles_dataset
X = StandardScaler().fit_transform(X)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.4, random_state=42)
alphas = np.logspace(-5, 1, 5)
current_subplot = plt.subplot(1, 5 + 1, 1)

cm = plt.cm.RdBu
cm_bright = ListedColormap(['#FF0000', '#0000FF'])
current_subplot.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright)
current_subplot.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)

def linear():
    model = LinearRegression().fit(X, y)
    print('Linear coefficient of determination:', model.score(X, y))
    y_ = model.predict(X)
    plt.plot(X, y_, color='blue', linewidth=1)
None

def polinomear():
    model = Pipeline([('poly', PolynomialFeatures(degree=5)), ('linear', LinearRegression())])
    model = model.fit(X, y)
    print('Polynomial coefficient of determination:', model.score(X, y))
    y_ = model.predict(X)
    plt.plot(X, y_, color='Red', linewidth=1)
None

def ridge():
    model = Pipeline([('poly', PolynomialFeatures(degree=5)), ('ridge', Ridge(alpha=1.0))])
    model = model.fit(X, y)
    print('Ridge coefficient of determination:', model.score(X, y))
    y_ = model.predict(X)
    plt.plot(X, y_, color='Purple', linewidth=1)
None

linear()
polinomear()
ridge()
plt.show()
