import matplotlib.pyplot as plt
from sklearn.datasets import make_moons
from sklearn.cluster import SpectralClustering

# Dataset
X = make_moons(noise=0.3, random_state=1)[0]

# Выполняем кластеризацию
cluster = SpectralClustering(n_clusters=4, assign_labels='discretize')
cluster.fit_predict(X)

# Print results
print(cluster.labels_)

# Разделяем набор данных и строим график
plt.scatter(X[:, 0], X[:, 1], c=cluster.labels_, cmap='rainbow')
plt.show()
