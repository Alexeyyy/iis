import pandas
import pandas as pd
from matplotlib import pyplot
from pandas import DataFrame
from sklearn import tree
from sklearn.datasets import data
from sklearn.tree import DecisionTreeClassifier
import numpy as np

#считываем файл
def read_file(path: str) -> pd.DataFrame:
    return pd.read_csv(path)

# Загружаем файл, выбираем столбцы по необходимым признакам
def get_data():
    return pd.DataFrame(data=read_file('titanic.csv'), columns=['Pclass', 'Cabin', 'Embarked', 'Survived'])

# Убираем пустые строки, если содержатся пропуски по любому из признаков
def delete_none():
        return dataframe[['Pclass', 'Cabin', 'Embarked', 'Survived']].dropna()

# Убираем буквы и оставляем только числа в номере кабины
def delete_let():
    dataframe['Cabin'].update(dataframe['Cabin'].str.replace(r'\D', '', regex=True))
    dataframe["Cabin"] = pd.to_numeric(dataframe['Cabin'])
    return dataframe

# Выводим названия признаков и их важность
def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)
# Визуализируем дерево решений
    tree.plot_tree(classifier)
    fig = pyplot.gcf()
    fig.set_size_inches(9, 7)
    pyplot.show()

dataframe = get_data()
dataframe = delete_let()
dataframe = delete_none()

# Выбираем данные
y = dataframe['Survived']
# Заменяем нечисловые значения признака Embarked, на числовые
x = dataframe[['Pclass', 'Cabin']].join(dataframe["Embarked"].map({"S": 1, "C": 2, "Q": 3}))
classifier = DecisionTreeClassifier()

# Обучение модели
classifier.fit(x, y)
print_results(classifier)