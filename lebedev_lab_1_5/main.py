import pandas as pd
from scipy import stats

DATA_FILE_NAME = 'titanic.csv'


def load_file(file):
    return pd.read_csv(file)


titanic_info = load_file(DATA_FILE_NAME)
print(f"Корреляция Пирсона SciPy: "
      f"{stats.pearsonr(titanic_info['SibSp'].values, titanic_info['Parch'].values)[0]}")

print(f"Корреляция Пирсона Pandas:"
      f"{titanic_info['SibSp'].corr(titanic_info['Parch'])}")
