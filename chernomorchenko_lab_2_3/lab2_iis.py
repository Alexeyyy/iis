# 2 лабораторная (столбцы по варианту - Sex,Age,SibSp).
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

COL_SURVIVED = 'Survived'
COL_SIBSP = 'SibSp'
COL_AGE = 'Age'
COL_SEX = 'Sex'
COL_PASS_ID = 'PassengerId'

def csv_read(file):
    return pd.read_csv(file, index_col=COL_PASS_ID)

def clear_empty_cells(data):
    return data[[COL_AGE, COL_SEX, COL_SIBSP, COL_SURVIVED]].dropna()

def replace_values(data):
    return data[[COL_AGE, COL_SEX, COL_SIBSP]].replace("female", 0).replace("male", 1)

data = csv_read('titanic.csv')
data_frame = pd.DataFrame(data=data, columns=[COL_AGE, COL_SEX, COL_SIBSP, COL_SURVIVED])
data_frame = clear_empty_cells(data_frame)
X = replace_values(data_frame)
clf = DecisionTreeClassifier()
Y = data_frame[COL_SURVIVED]
clf.fit(X, Y)
print("Признаки:")
print(clf.feature_names_in_)
print("Важность признаков:")
print(list(map(lambda x : round(x, 5), clf.feature_importances_)))

