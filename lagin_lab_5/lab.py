import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift
from sklearn.datasets import make_circles

# Получаем X из датасета
X = make_circles(noise=0.2, random_state=1)[0]

# Создаем модель кластера и выполняем кластеризацию
cluster = MeanShift()
cluster.fit_predict(X)

# Выводим результаты
print(cluster.labels_)

plt.scatter(X[:, 0], X[:, 1], c=cluster.labels_, cmap='rainbow')
plt.show()
