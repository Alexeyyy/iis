import matplotlib.pyplot as plt
from sklearn import cluster, datasets


def show_plt():
    plt.figure(1)
    plt.subplot(1, 2, 1)
    plt.title('Набор данных')
    plt.grid(True)
    plt.scatter(noisy_moons[:, 0], noisy_moons[:, 1], c=moon_labels)
    plt.subplot(1, 2, 2)
    plt.title("Кластеризация DBSCAN")
    plt.grid(True)
    plt.scatter(noisy_moons[:, 0], noisy_moons[:, 1], c=dbs.labels_)
    plt.show()


noisy_moons, moon_labels = datasets.make_moons(n_samples=2000, noise=.1)
dbs = cluster.DBSCAN(eps=0.1)
dbs.fit(noisy_moons)
show_plt()
