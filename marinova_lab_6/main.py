import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

GENDER = 'gender'
STYLE  = 'style'
DATASET = 'music_recomender.csv'
STOP_WORDS = 'english'

data = pd.read_csv(DATASET, low_memory=False)

tf_idf = TfidfVectorizer(stop_words=STOP_WORDS)
data[GENDER] = data[GENDER].fillna('')
matrix = tf_idf.fit_transform(data[GENDER])
cosine = linear_kernel(matrix, matrix)
indices = pd.Series(data.index, index=data[STYLE]).drop_duplicates()

def get_recom(title):
    x = indices[title]
    scores = list(enumerate(cosine[x]))
    scores = sorted(scores, key=lambda x: x[1], reverse=True)
    sim_scores = scores[1:11]
    music_indices = [i[0] for i in sim_scores]
    return data[STYLE].iloc[music_indices]

print(get_recom('Dance'))
