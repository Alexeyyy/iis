import csv

def to_fixed(num_obj, digits=0):
    return f"{num_obj:.{digits}f}"

def data_reader(file_obj):
    reader = csv.DictReader(file_obj, delimiter=',')
    counter = 0
    res = 0
    for line in reader:
        counter += 1
        res += int(line['Survived'])
    print (to_fixed(100 * res/counter, 2) + '% выжили из ' + str(counter) + ' человек')


if __name__ == "__main__":
    with open("titanic.csv") as f_obj:
        data_reader(f_obj)
