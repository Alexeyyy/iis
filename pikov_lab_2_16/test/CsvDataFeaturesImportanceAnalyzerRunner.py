import unittest

from pikov_lab_2_16.CsvDataFeaturesImportanceAnalyzer import CsvDataFeaturesImportanceAnalyzer

IS_SURVIVED_COLUMN_NAME = 'Survived'  # выжил ли пассажир
SIB_SP_COLUMN_NAME = 'SibSp'  # количество родственников 2-го порядка (муж, жена, братья, сетры)
PARCH_COLUMN_NAME = 'Parch'  # количество родственников 1-го порядка (мать, отец, дети)
TICKET_COLUMN_NAME = 'Ticket'  # номер билета


class CsvDataFeaturesImportanceAnalyzerRunner(unittest.TestCase):

    def test_print_16_variant_features_importance(self):
        data_file_name = '../titanic.csv'
        researchable_columns = [SIB_SP_COLUMN_NAME, PARCH_COLUMN_NAME, TICKET_COLUMN_NAME]
        target_column = IS_SURVIVED_COLUMN_NAME

        analyzer = CsvDataFeaturesImportanceAnalyzer(
            csv_file_name=data_file_name,
            researchable_column_names=researchable_columns,
            target_column_name=target_column
        )

        analyzer.print_features_importance()
