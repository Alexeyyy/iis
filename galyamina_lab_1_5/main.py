import pandas as pd
import numpy as np

SIBSP = 'SibSp'
PARCH = 'Parch'
table = pd.read_csv('titanic.csv')
SibSp = table[SIBSP]
Parch = table[PARCH]

#Проверка, есть ли шумы и пропуски
def check_data():

    _SibSp = table.groupby(SIBSP).SibSp.count()
    _Parch = table.groupby(PARCH).Parch.count()
    print(_SibSp)
    print(_Parch)

#вычисляем коэффициент корреляции
def find_correlation():
    print(np.corrcoef(Parch, SibSp)[0, 1])

check_data()
find_correlation()


