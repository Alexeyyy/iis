import pandas as pd
from pandas import DataFrame

# Считываем данные из файла
COLUMN_SEX = "Sex"
COLUMN_PCLASS = "Pclass"
data = pd.read_csv('titanic.csv')
dataFrame = pd.DataFrame(data)


# Считаем строки, которые удовлетворяют условию
def get_count_women(passengers) -> int:
    count = 0
    for _, row in passengers.iterrows():
        if str(row[COLUMN_SEX]) == 'female' and str(row[COLUMN_PCLASS]) == '1':
            count += 1
    return count


def get_count_men():
    df = dataFrame.loc[dataFrame[COLUMN_SEX] == 'male']
    count = df.shape[0]
    return count


# Вывод количества женщин, плывших
# в 1 классе
print('Количество женщин, плывших в 1 классе: ', get_count_women(data))
# Вывод количества мужчин
print('Количество мужчин: ', get_count_men())
