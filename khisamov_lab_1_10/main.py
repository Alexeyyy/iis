# 10.Найдите имя самого молодого человека.

import pandas as pd


def get_name_of_human_with_min_age() -> str:
    titanic = pd.read_csv('titanic.csv', delimiter=',')
    min_age = float(titanic.Age.min())  # находим минимальный возраст пассажира
    min_age_str = titanic["Age"] == min_age  # проверяем какая строка соответсвует минимальному возрасту
    min_name = titanic.loc[min_age_str]  # получаем строку с пассажиром, у которого минимальный возраст
    id_str = int(min_name.index.values)  # номер строки пассажира
    min_name = titanic.loc[id_str].at["Name"]  # имя пассажира
    return "Name: " + str(min_name) + " Age: " + str(min_age)


print(get_name_of_human_with_min_age())
