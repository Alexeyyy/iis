import pandas as p
from sklearn.tree import DecisionTreeClassifier

# Считываем данные из файла
DATA = 'titanic.csv'
COLUMN_SURVIVED = "Survived"
COLUMN_PARCH = "Parch"
COLUMN_TICKET = "Ticket"
COLUMN_FARE = "Fare"
data = p.read_csv(DATA)


# Удаляем строки, где есть пустое значение
def drop_values(data):
    return data[[COLUMN_SURVIVED, COLUMN_PARCH, COLUMN_TICKET, COLUMN_FARE]].dropna()


# В номере билета убираем все буквы и знаки
def drop_letters(str):
    res = ""
    for i in str:
        if i.isdigit():
            res = res + i
    return res


# Заменяем старый номер билета на новый,
# состоящий только из цифр
def new_ticket_number(data):
    for i in range(len(data[COLUMN_TICKET])):
        data[COLUMN_TICKET].update(data[COLUMN_TICKET].replace(data[COLUMN_TICKET][i], drop_letters(data[COLUMN_TICKET][i])))
    data[COLUMN_TICKET] = p.to_numeric(data[COLUMN_TICKET])
    return data


# Выборка данных, обучение модели и вывод
# важности признаков
def model_training(data):
    data = new_ticket_number(data)
    data = drop_values(data)
    y = data[COLUMN_SURVIVED]
    x = data[[COLUMN_PARCH, COLUMN_TICKET, COLUMN_FARE]]
    clf = DecisionTreeClassifier()
    clf.fit(x, y)
    print(clf.feature_names_in_)
    print(clf.feature_importances_)


def main():
    model_training(data)


main()
