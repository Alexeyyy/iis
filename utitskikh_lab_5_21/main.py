from scipy.cluster.hierarchy import linkage, dendrogram
import matplotlib.pyplot as plt
import pandas as pd


seeds_df = pd.read_csv('SC2_rating.csv')

varieties = list(seeds_df.pop('Race'))

samples = seeds_df.values

hierarchical_clustering = linkage(samples, method='complete')
print(hierarchical_clustering)

dendrogram(hierarchical_clustering,
           labels=varieties,
           leaf_rotation=90,
           leaf_font_size=6,
           )

plt.show()
