import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as pyplot


FILE_NAME = 'titanic.csv'
NAME = 'Name'
CABIN = 'Cabin'
EMB = 'Embarked'
IS_SURV = 'Survived'
fig_width = 9
fig_height = 6


def load_file(file) -> DataFrame:
    return pd.read_csv(file)


def get_dataframe():
    return pd.DataFrame(data=load_file(FILE_NAME), columns=[NAME, CABIN, EMB, IS_SURV])


def delete_none():
    return dataframe[[NAME, CABIN, EMB, IS_SURV]].dropna()


def reformat_name(name):
    result = 1
    for letter in name:
        result += ord(letter) + result % 10
    return result


def delete_letters():
    dataframe[CABIN].update(dataframe[CABIN].str.replace(r'\D', '', regex=True))
    dataframe[CABIN] = pd.to_numeric(dataframe[CABIN])
    return dataframe


def print_results(classifier):
    print(classifier.feature_names_in_)
    print(classifier.feature_importances_)

    tree.plot_tree(classifier)
    fig = pyplot.gcf()
    fig.set_size_inches(fig_width, fig_height)
    pyplot.show()


dataframe = get_dataframe()
dataframe = delete_letters()
dataframe[NAME] = dataframe[NAME].apply(reformat_name)
dataframe = delete_none()

# print(dataframe)

y = dataframe[IS_SURV]
x = dataframe[[NAME, CABIN]].join(dataframe[EMB].map({"S": 3, "C": 1, "Q": 2}))

classifier = DecisionTreeClassifier()

classifier.fit(x, y)

print_results(classifier)
