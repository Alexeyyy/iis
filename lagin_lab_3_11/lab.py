# make_circles (noise=0.2, factor=0.5, random_state=rs)
# Линейная регрессия
# Полиномиальную регрессия(со степенью 4)
# Персептрон

import numpy as np
from sklearn.datasets import make_circles
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression, Perceptron
from sklearn.preprocessing import PolynomialFeatures


def do_linear_regression(x_train, y_train, x_test, y_test):
    model = LinearRegression().fit(x_train, y_train)
    predicted_y = model.predict(x_test)

    print('Линейный коэффициент детерминации:', model.score(x_train, y_train))
    plt.title("График линейной регрессии")
    plt.scatter(x_test, y_test, color='black')
    plt.plot(x_test, predicted_y, color='blue', linewidth=3)
    plt.show()


def do_polynomial_regression(x_train, y_train):
    features = PolynomialFeatures(degree=4)
    x_poly = features.fit_transform(x_train)

    regression = LinearRegression()
    regression.fit(x_poly, y_train)

    print('Полиномиальный коэффициент детерминации:', regression.score(x_poly, y_train))
    plt.title("График полиномиальной регрессии")
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, regression.predict(x_poly), color='red', linewidth=3)
    plt.show()


def do_perceptron(x_train, y_train, x_test):
    perceptron = Perceptron()
    perceptron.fit(x_train, y_train.ravel())

    print('Средняя точность:', perceptron.score(x_train, y_train))
    plt.title("График персептрона")
    plt.scatter(x_train, y_train, color='blue')
    plt.scatter(x_train, perceptron.predict(x_test), color='red', linewidth=3)
    plt.show()


# Подгружаем датасет и распределяем данные на X, Y
dataset = make_circles(noise=0.2, random_state=1, factor=0.5)
print(dataset)

x = np.array(dataset[0])
y = np.array(dataset[1])

# Преобразуем двумерный массив в одномерный
x = x.reshape(-1, 1)
y = y.reshape(-1, 1)

# 50 значений для тренировки, 50 для теста
x_train, x_test = x[:50], x[-50:]
y_train, y_test = y[:50], y[-50:]

do_linear_regression(x_train, y_train, x_test, y_test)
do_polynomial_regression(x_train, y_train)
do_perceptron(x_train, y_train, x_test)


