import pandas as pd
import numpy as np
import scipy as sp
from sklearn.metrics.pairwise import cosine_similarity


def read_file(file_name):
    return pd.read_csv(file_name)


def remove_ratings_user(rating):
    rating.rating.replace({-1: np.nan}, regex=True, inplace = True)
    return rating.head()


def choose_type(anime, COLUMN_TYPE, SHOW_TYPE_MOVIE):
    anime_type = anime[anime[COLUMN_TYPE]==SHOW_TYPE_MOVIE]
    return anime_type


def merge_dataframes(rating, anime_type, COLUMN_ANIME_NAME, COLUMN_ANIME_ID, COLUMN_USER_ID, COLUMN_USER_RATING, COLUMN_USER_NEW_RATING):
    merged = rating.merge(anime_type, left_on = COLUMN_ANIME_ID, right_on = COLUMN_ANIME_ID, suffixes= ['_user', ''])
    merged.rename(columns = {COLUMN_USER_NEW_RATING:COLUMN_USER_RATING}, inplace = True)
    merged=merged[[COLUMN_USER_ID, COLUMN_ANIME_NAME, COLUMN_USER_RATING]]
    merged_sub= merged[merged.user_id <= 10000]
    return merged_sub

# Нормализация значений, удаляем произведения без оценок
def pivot_norm_table(merged_sub, COLUMN_USER_ID, COLUMN_ANIME_NAME, COLUMN_USER_RATING):
    piv = merged_sub.pivot_table(index=[COLUMN_USER_ID], columns=[COLUMN_ANIME_NAME], values=COLUMN_USER_RATING)
    piv_norm = piv.apply(lambda x: (x-np.mean(x))/(np.max(x)-np.min(x)), axis=1)
    piv_norm.fillna(0, inplace=True)
    piv_norm = piv_norm.T
    piv_norm = piv_norm.loc[:, (piv_norm != 0).any(axis=0)]
    return piv_norm

# Считаем сходство значений
def sparse_table(piv_norm):
    piv_sparse = sp.sparse.csr_matrix(piv_norm.values)
    item_similarity = cosine_similarity(piv_sparse)
    item_sim_df = pd.DataFrame(item_similarity, index = piv_norm.index, columns = piv_norm.index)
    return item_sim_df
    

def top_animes(anime_name, item_sim_df):
    count = 1
    print('Аниме похожие на {}:\n'.format(anime_name))
    for item in item_sim_df.sort_values(by = anime_name, ascending = False).index[1:11]:
        print('No. {}: {}'.format(count, item))
        count +=1  


def main():
    SHOW_TYPE_MOVIE = 'Movie'

    COLUMN_TYPE = 'type'
    COLUMN_ANIME_NAME = 'name'
    COLUMN_ANIME_ID = 'anime_id'
    COLUMN_USER_ID = 'user_id'
    COLUMN_USER_RATING = 'user_rating'
    COLUMN_USER_NEW_RATING = 'rating_user'

    anime = pd.read_csv('anime.csv')
    rating = pd.read_csv('rating.csv')

    remove_ratings_user(rating)
    anime_type = choose_type(anime, COLUMN_TYPE, SHOW_TYPE_MOVIE)
    merged_frames = merge_dataframes(rating, anime_type, COLUMN_ANIME_NAME, COLUMN_ANIME_ID, COLUMN_USER_ID, COLUMN_USER_RATING, COLUMN_USER_NEW_RATING)
    norm_table = pivot_norm_table(merged_frames, COLUMN_USER_ID, COLUMN_ANIME_NAME, COLUMN_USER_RATING)
    item = sparse_table(norm_table)

    top_animes('Neon Genesis Evangelion: The End of Evangelion', item)


if __name__ == '__main__':
    main()
