
# Методы ранжирования признаков по варианту:
# Лассо (Lasso)
# Рекурсивное сокращение признаков (Recursive Feature Elimination – RFE)
# Сокращение признаков Случайными деревьями (Random Forest Regressor)

import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import Lasso
from sklearn.feature_selection import RFE
from sklearn.ensemble import RandomForestRegressor


def freedman_problem(size):
    X = np.random.uniform(0, 1, (size, 14))
    # Функция-выход проблемы Фридмана
    Y = (10 * np.sin(np.pi * X[:, 0] * X[:, 1]) + 20 * (X[:, 2] - .5) ** 2 +
         10 * X[:, 3] + 5 * X[:, 4] ** 5 + np.random.normal(0, 1))
    # Добавляем зависимость признаков
    X[:, 10:] = X[:, :4] + np.random.normal(0, .025, (size, 4))
    return X, Y


# Принимаем готовый список и список оценок по признакам
# Отдаём словарь из попарно сопоставленных элементов списков
def rank_to_dict(ranks, names):
    ranks = np.abs(ranks)
    minmax = MinMaxScaler()
    ranks = minmax.fit_transform(np.array(ranks).reshape(14, 1)).ravel()
    ranks = map(lambda x: round(x, 2), ranks)
    return dict(zip(names, ranks))


def sort(data):
    sorted_values = sorted(data.values())
    sorted_data = {}

    for i in sorted_values:
        for j in data.keys():
            if data[j] == i:
                sorted_data[j] = data[j]
                break
    return sorted_data


# Генерируем 750 строк-наблюдений и 14 столбцов-признаков
np.random.seed(0)
size = 750

print(freedman_problem(size))
X, Y = freedman_problem(size)

# Метод Лассо
lasso = Lasso(alpha=.001)
lasso.fit(X,  Y)

# Метод рекурсивного сокращения признаков
rfe = RFE(lasso)
rfe.fit(X, Y)

# Метод сокращения признаков Случайными деревьями
rfr = RandomForestRegressor(random_state=0)
rfr.fit(X, Y)

names = ["x%s" % i for i in range(1, 15)]
rank = [rank_to_dict(lasso.coef_, names),
        rank_to_dict(rfr.feature_importances_, names), ]

for i in range(len(rank)):
    print(sort(rank[i]))

print(rank_to_dict(rfe.support_, names))