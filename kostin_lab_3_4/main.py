import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.datasets import make_moons, make_circles, make_classification

def genarate_date():
    return make_moons(
        noise=0.,
        random_state=1)

def linear(X_train, X_test, y_train, y_test):
    model = LinearRegression().fit(X_train, y_train)
    y_pred = model.predict(X_test)

    print('Linear')
    print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
    print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))

    plt.scatter(X_test, y_test, color='black')
    plt.plot(X_test, y_pred, color='blue', linewidth=3)
    plt.show()

def polynomial(X,y):
    poly_reg = PolynomialFeatures(degree=3)
    X_poly = poly_reg.fit_transform(X)
    pol_reg = LinearRegression()
    pol_reg.fit(X_poly, y)
    y_pred = pol_reg.predict(X_poly)

    print('Polynomial')
    print('Mean Absolute Error:', metrics.mean_absolute_error(y, y_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(y, y_pred))
    print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y, y_pred)))

    plt.scatter(X, y, color='red')
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
    plt.show()

def ridge_polynomial(X_train, X_test, y_train, y_test):
    clf = Ridge(alpha=1.0)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    print('RidgePolynomial')
    print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
    print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
    print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))

    plt.scatter(X_test, y_test, color='black')
    plt.plot(X_test, y_pred, color='blue', linewidth=3)
    plt.show()


X,y =genarate_date()
X = X[:, np.newaxis, 1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.4, random_state=42)
linear(X_train, X_test, y_train, y_test)
polynomial(X,y)
ridge_polynomial(X_train, X_test, y_train, y_test)

